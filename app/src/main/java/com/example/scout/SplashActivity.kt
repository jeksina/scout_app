package com.example.scout

import android.content.Intent
import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.TextView

class SplashActivity : AppCompatActivity() {

    lateinit var a: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        init()
    }

    fun init() {

        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        delayedStartActivity()
    }

    fun delayedStartActivity() {
        Handler().postDelayed({
            val intent = Intent(this@SplashActivity, LogInActivity::class.java)
            startActivity(intent)
            finish()
        }, SPLASH_TIME_OUT.toLong())
    }

    companion object {
        const val SPLASH_TIME_OUT = 1000
    }
}