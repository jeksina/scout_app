package com.example.scout.Data

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "User_Table")
data class UserEntity(
    @PrimaryKey(autoGenerate = true)
    var id :Int,
    var firstName :String,
    var lastName :String,
    var emailAddress :String,
    var phoneNo :String,
    var password :String,
    var confirmPassword :String,
    var address :String
): Parcelable