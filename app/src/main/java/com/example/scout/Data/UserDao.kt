package com.example.scout.Data

import androidx.room.*

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun addUser(userEntity: UserEntity)

    @Query("SELECT * FROM User_Table WHERE emailAddress=(:emailAddress) AND password=(:password)")
    fun login(emailAddress: String, password: String): UserEntity

    @Query("SELECT * FROM User_Table WHERE emailAddress=(:emailAddress) AND phoneNo=(:phoneNo)")
    fun Forgot_Password(emailAddress: String, phoneNo: String): UserEntity

    @Query("UPDATE User_Table SET password=(:password) WHERE id=(:id)")
    fun Password_Update(password: Int, id: String)
}