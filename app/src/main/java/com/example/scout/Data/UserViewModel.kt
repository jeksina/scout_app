package com.example.scout.Data

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class UserViewModel(application: Application) :AndroidViewModel(application) {

 //   private var readAllData :LiveData<List<UserEntity>>
    private var repository :UserRepository

    init {
        var userDao = UserDatabase.getDatabase(application).userDao()
        repository = UserRepository(userDao)
  //      readAllData = repository.readAllData
    }

    fun addUser(userEntity: UserEntity){
        viewModelScope.launch(Dispatchers.IO){
            repository.addUser(userEntity)
        }
    }


}