package com.example.scout.Data

import androidx.lifecycle.LiveData

class UserRepository(private var userDao: UserDao) {

 //   var readAllData :LiveData<List<UserEntity>> = userDao.readAllData()

    fun addUser(userEntity: UserEntity) {
        userDao.addUser(userEntity)
    }

}