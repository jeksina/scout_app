package com.example.scout

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.navigation.fragment.findNavController
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.jaredrummler.materialspinner.MaterialSpinner
import kotlin.system.exitProcess

class ProductionFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view: View = inflater.inflate(R.layout.fragment_production, container, false)

        var Production_calender = view.findViewById<LinearLayout>(R.id.tvProductionCalender)
        var Productionimage_calender = view.findViewById<ImageView>(R.id.ivProductionCalender)
        var Production_spinner = view.findViewById<MaterialSpinner>(R.id.spProductionCalender)

        var View_Pager = view.findViewById<ViewPager>(R.id.Production_viewPager)
        var Tab = view.findViewById<TabLayout>(R.id.Production_tabs)

        var Production_mon = view.findViewById<LinearLayout>(R.id.Production_mon)
        var Production_tue = view.findViewById<LinearLayout>(R.id.Production_tue)
        var Production_wed = view.findViewById<LinearLayout>(R.id.Production_wed)
        var Production_thurs = view.findViewById<LinearLayout>(R.id.Production_thurs)
        var Production_fri = view.findViewById<LinearLayout>(R.id.Production_fri)
        var Production_sat = view.findViewById<LinearLayout>(R.id.Production_sat)
        var Production_sun = view.findViewById<LinearLayout>(R.id.Production_sat)

        var Back = view.findViewById<ImageView>(R.id.Production_back)

        Back.setOnClickListener {

            var action = ProductionFragmentDirections.actionProductionFragmentToMenuFragment()
            findNavController().navigate(action)
        }

        Production_calender.visibility = View.GONE

        Productionimage_calender.setOnClickListener{
            Productionimage_calender.setColorFilter(resources.getColor(com.example.scout.R.color.green))
            Production_calender.visibility = View.VISIBLE

            val SPINNER_DATA = arrayOf("Month", "Month to date", "Last Month", "Year to date" , "Custom")

            val adapter: ArrayAdapter<String>? = this!!.activity?.let { it1 ->
                ArrayAdapter<String>(
                    it1,
                    android.R.layout.simple_dropdown_item_1line,
                    SPINNER_DATA
                )
            }

            if (adapter != null) {
                Production_spinner.setAdapter(adapter)
            }
        }

        val adapter = MyViewPagerAdapter(childFragmentManager)
        adapter.addFragment(ProductionDataFragment() , " All Provider ")
        adapter.addFragment(ProductionDataFragment() , " Dr.John ")
        adapter.addFragment(ProductionDataFragment() , " Dr.Smith ")
        adapter.addFragment(ProductionDataFragment() , "Dr.Williab ")
        View_Pager.adapter = adapter
        Tab.setupWithViewPager(View_Pager)

        Production_mon.setOnClickListener {
            Production_mon.setBackgroundResource(com.example.scout.R.drawable.round_border);
            Production_tue.setBackgroundResource(com.example.scout.R.color.calender_back);
            Production_wed.setBackgroundResource(com.example.scout.R.color.calender_back);
            Production_thurs.setBackgroundResource(com.example.scout.R.color.calender_back);
            Production_fri.setBackgroundResource(com.example.scout.R.color.calender_back);
            Production_sat.setBackgroundResource(com.example.scout.R.color.calender_back);
            Production_sun.setBackgroundResource(com.example.scout.R.color.calender_back);
        }

        Production_tue.setOnClickListener {
            Production_mon.setBackgroundResource(com.example.scout.R.color.calender_back);
            Production_tue.setBackgroundResource(com.example.scout.R.drawable.round_border);
            Production_wed.setBackgroundResource(com.example.scout.R.color.calender_back);
            Production_thurs.setBackgroundResource(com.example.scout.R.color.calender_back);
            Production_fri.setBackgroundResource(com.example.scout.R.color.calender_back);
            Production_sat.setBackgroundResource(com.example.scout.R.color.calender_back);
            Production_sun.setBackgroundResource(com.example.scout.R.color.calender_back);
        }

        Production_wed.setOnClickListener {
            Production_mon.setBackgroundResource(com.example.scout.R.color.calender_back);
            Production_tue.setBackgroundResource(com.example.scout.R.color.calender_back);
            Production_wed.setBackgroundResource(com.example.scout.R.drawable.round_border);
            Production_thurs.setBackgroundResource(com.example.scout.R.color.calender_back);
            Production_fri.setBackgroundResource(com.example.scout.R.color.calender_back);
            Production_sat.setBackgroundResource(com.example.scout.R.color.calender_back);
            Production_sun.setBackgroundResource(com.example.scout.R.color.calender_back);
        }

        Production_thurs.setOnClickListener {
            Production_mon.setBackgroundResource(com.example.scout.R.color.calender_back);
            Production_tue.setBackgroundResource(com.example.scout.R.color.calender_back);
            Production_wed.setBackgroundResource(com.example.scout.R.color.calender_back);
            Production_thurs.setBackgroundResource(com.example.scout.R.drawable.round_border);
            Production_fri.setBackgroundResource(com.example.scout.R.color.calender_back);
            Production_sat.setBackgroundResource(com.example.scout.R.color.calender_back);
            Production_sun.setBackgroundResource(com.example.scout.R.color.calender_back);
        }

        Production_fri.setOnClickListener {
            Production_mon.setBackgroundResource(com.example.scout.R.color.calender_back);
            Production_tue.setBackgroundResource(com.example.scout.R.color.calender_back);
            Production_wed.setBackgroundResource(com.example.scout.R.color.calender_back);
            Production_thurs.setBackgroundResource(com.example.scout.R.color.calender_back);
            Production_fri.setBackgroundResource(com.example.scout.R.drawable.round_border);
            Production_sat.setBackgroundResource(com.example.scout.R.color.calender_back);
            Production_sun.setBackgroundResource(com.example.scout.R.color.calender_back);
        }

        Production_sat.setOnClickListener {
            Production_mon.setBackgroundResource(com.example.scout.R.color.calender_back);
            Production_tue.setBackgroundResource(com.example.scout.R.color.calender_back);
            Production_wed.setBackgroundResource(com.example.scout.R.color.calender_back);
            Production_thurs.setBackgroundResource(com.example.scout.R.color.calender_back);
            Production_fri.setBackgroundResource(com.example.scout.R.color.calender_back);
            Production_sat.setBackgroundResource(com.example.scout.R.drawable.round_border);
            Production_sun.setBackgroundResource(com.example.scout.R.color.calender_back);
        }

        Production_sun.setOnClickListener {
            Production_mon.setBackgroundResource(com.example.scout.R.color.calender_back);
            Production_tue.setBackgroundResource(com.example.scout.R.color.calender_back);
            Production_wed.setBackgroundResource(com.example.scout.R.color.calender_back);
            Production_thurs.setBackgroundResource(com.example.scout.R.color.calender_back);
            Production_fri.setBackgroundResource(com.example.scout.R.color.calender_back);
            Production_sat.setBackgroundResource(com.example.scout.R.color.calender_back);
            Production_sun.setBackgroundResource(com.example.scout.R.drawable.round_border);
        }

        return view
    }

    class MyViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {

        private val fragmentList: MutableList<Fragment> = ArrayList()
        private val titleList: MutableList<String> = ArrayList()

        override fun getItem(position: Int): Fragment {
            return fragmentList[position]
        }

        override fun getCount(): Int {
            return fragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String) {
            fragmentList.add(fragment)
            titleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return titleList[position]
        }
    }

}