package com.example.scout

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_setting.*
import kotlinx.android.synthetic.main.fragment_setting.view.*


class SettingFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view :View = inflater.inflate(R.layout.fragment_setting, container, false)

        var Change_Password = view.findViewById<LinearLayout>(R.id.change_password)
        var Back_Arrow = view.findViewById<ImageView>(R.id.ivSetting_backarrow)

        Change_Password.setOnClickListener {
            var action = SettingFragmentDirections.actionSettingFragmentToChangePassWorldFragment()
            findNavController().navigate(action)
        }

        Back_Arrow.setOnClickListener {

            var action = SettingFragmentDirections.actionSettingFragmentToMenuFragment()
            findNavController().navigate(action)
        }

        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                var action = SettingFragmentDirections.actionSettingFragmentToMenuFragment()
                findNavController().navigate(action)
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner,callback)

        return view
    }
}