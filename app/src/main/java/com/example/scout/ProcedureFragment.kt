package com.example.scout

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.navigation.fragment.findNavController
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.jaredrummler.materialspinner.MaterialSpinner

class ProcedureFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view :View = inflater.inflate(R.layout.fragment_procedure, container, false)

        var Procedure_calender = view.findViewById<LinearLayout>(R.id.tvProcedure_Calender)
        var Procedureimage_calender = view.findViewById<ImageView>(R.id.ivProcedure_Calender)
        var Procedure_spinner = view.findViewById<MaterialSpinner>(R.id.spProcedure_Calender)

        var View_Pager = view.findViewById<ViewPager>(R.id.procedure_viewPager)
        var Tab = view.findViewById<TabLayout>(R.id.procedure_tabs)

        var Procedure_mon = view.findViewById<LinearLayout>(R.id.Procedure_mon)
        var Procedure_tue = view.findViewById<LinearLayout>(R.id.Procedure_tue)
        var Procedure_wed = view.findViewById<LinearLayout>(R.id.Procedure_wed)
        var Procedure_thurs = view.findViewById<LinearLayout>(R.id.Procedure_thurs)
        var Procedure_fri = view.findViewById<LinearLayout>(R.id.Procedure_fri)
        var Procedure_sat = view.findViewById<LinearLayout>(R.id.Procedure_sat)
        var Procedure_sun = view.findViewById<LinearLayout>(R.id.Procedure_sun)

        var Back = view.findViewById<ImageView>(R.id.Procedure_Back)

        Back.setOnClickListener {

            var action = ProcedureFragmentDirections.actionProcedureFragmentToMenuFragment()
            findNavController().navigate(action)
        }


        Procedure_calender.visibility = View.GONE

        Procedureimage_calender.setOnClickListener{
            Procedureimage_calender.setColorFilter(resources.getColor(com.example.scout.R.color.green))
            Procedure_calender.visibility = View.VISIBLE

            val SPINNER_DATA = arrayOf("Month", "Month to date", "Last Month", "Year to date" , "Custom")

            val adapter: ArrayAdapter<String>? = this!!.activity?.let { it1 ->
                ArrayAdapter<String>(
                    it1,
                    android.R.layout.simple_dropdown_item_1line,
                    SPINNER_DATA
                )
            }

            if (adapter != null) {
                Procedure_spinner.setAdapter(adapter)
            }
        }

        val adapter = MyViewPagerAdapter(childFragmentManager)
        adapter.addFragment(ProcedureDataFragment() , " All Provider ")
        adapter.addFragment(ProcedureDataFragment() , " Dr.John ")
        adapter.addFragment(ProcedureDataFragment() , " Dr.Smith ")
        adapter.addFragment(ProcedureDataFragment() , "Dr.Williab ")
        View_Pager.adapter = adapter
        Tab.setupWithViewPager(View_Pager)

        Procedure_mon.setOnClickListener {
            Procedure_mon.setBackgroundResource(com.example.scout.R.drawable.round_border);
            Procedure_tue.setBackgroundResource(com.example.scout.R.color.calender_back);
            Procedure_wed.setBackgroundResource(com.example.scout.R.color.calender_back);
            Procedure_thurs.setBackgroundResource(com.example.scout.R.color.calender_back);
            Procedure_fri.setBackgroundResource(com.example.scout.R.color.calender_back);
            Procedure_sat.setBackgroundResource(com.example.scout.R.color.calender_back);
            Procedure_sun.setBackgroundResource(com.example.scout.R.color.calender_back);
        }

        Procedure_tue.setOnClickListener {
            Procedure_mon.setBackgroundResource(com.example.scout.R.color.calender_back);
            Procedure_tue.setBackgroundResource(com.example.scout.R.drawable.round_border);
            Procedure_wed.setBackgroundResource(com.example.scout.R.color.calender_back);
            Procedure_thurs.setBackgroundResource(com.example.scout.R.color.calender_back);
            Procedure_fri.setBackgroundResource(com.example.scout.R.color.calender_back);
            Procedure_sat.setBackgroundResource(com.example.scout.R.color.calender_back);
            Procedure_sun.setBackgroundResource(com.example.scout.R.color.calender_back);
        }

        Procedure_wed.setOnClickListener {
            Procedure_mon.setBackgroundResource(com.example.scout.R.color.calender_back);
            Procedure_tue.setBackgroundResource(com.example.scout.R.color.calender_back);
            Procedure_wed.setBackgroundResource(com.example.scout.R.drawable.round_border);
            Procedure_thurs.setBackgroundResource(com.example.scout.R.color.calender_back);
            Procedure_fri.setBackgroundResource(com.example.scout.R.color.calender_back);
            Procedure_sat.setBackgroundResource(com.example.scout.R.color.calender_back);
            Procedure_sun.setBackgroundResource(com.example.scout.R.color.calender_back);
        }

        Procedure_thurs.setOnClickListener {
            Procedure_mon.setBackgroundResource(com.example.scout.R.color.calender_back);
            Procedure_tue.setBackgroundResource(com.example.scout.R.color.calender_back);
            Procedure_wed.setBackgroundResource(com.example.scout.R.color.calender_back);
            Procedure_thurs.setBackgroundResource(com.example.scout.R.drawable.round_border);
            Procedure_fri.setBackgroundResource(com.example.scout.R.color.calender_back);
            Procedure_sat.setBackgroundResource(com.example.scout.R.color.calender_back);
            Procedure_sun.setBackgroundResource(com.example.scout.R.color.calender_back);
        }

        Procedure_fri.setOnClickListener {
            Procedure_mon.setBackgroundResource(com.example.scout.R.color.calender_back);
            Procedure_tue.setBackgroundResource(com.example.scout.R.color.calender_back);
            Procedure_wed.setBackgroundResource(com.example.scout.R.color.calender_back);
            Procedure_thurs.setBackgroundResource(com.example.scout.R.color.calender_back);
            Procedure_fri.setBackgroundResource(com.example.scout.R.drawable.round_border);
            Procedure_sat.setBackgroundResource(com.example.scout.R.color.calender_back);
            Procedure_sun.setBackgroundResource(com.example.scout.R.color.calender_back);
        }

        Procedure_sat.setOnClickListener {
            Procedure_mon.setBackgroundResource(com.example.scout.R.color.calender_back);
            Procedure_tue.setBackgroundResource(com.example.scout.R.color.calender_back);
            Procedure_wed.setBackgroundResource(com.example.scout.R.color.calender_back);
            Procedure_thurs.setBackgroundResource(com.example.scout.R.color.calender_back);
            Procedure_fri.setBackgroundResource(com.example.scout.R.color.calender_back);
            Procedure_sat.setBackgroundResource(com.example.scout.R.drawable.round_border);
            Procedure_sun.setBackgroundResource(com.example.scout.R.color.calender_back);
        }

        Procedure_sun.setOnClickListener {
            Procedure_mon.setBackgroundResource(com.example.scout.R.color.calender_back);
            Procedure_tue.setBackgroundResource(com.example.scout.R.color.calender_back);
            Procedure_wed.setBackgroundResource(com.example.scout.R.color.calender_back);
            Procedure_thurs.setBackgroundResource(com.example.scout.R.color.calender_back);
            Procedure_fri.setBackgroundResource(com.example.scout.R.color.calender_back);
            Procedure_sat.setBackgroundResource(com.example.scout.R.color.calender_back);
            Procedure_sun.setBackgroundResource(com.example.scout.R.drawable.round_border);
        }

        return view
    }

    class MyViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {

        private val fragmentList: MutableList<Fragment> = ArrayList()
        private val titleList: MutableList<String> = ArrayList()

        override fun getItem(position: Int): Fragment {
            return fragmentList[position]
        }

        override fun getCount(): Int {
            return fragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String) {
            fragmentList.add(fragment)
            titleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return titleList[position]
        }
    }
}