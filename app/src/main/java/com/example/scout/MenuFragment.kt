package com.example.scout

import android.app.SearchManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.navigation.fragment.findNavController
import androidx.viewpager.widget.ViewPager
import com.google.android.material.navigation.NavigationView
import com.google.android.material.tabs.TabLayout
import com.jaredrummler.materialspinner.MaterialSpinner
import kotlinx.android.synthetic.main.fragment_menu.view.*


class MenuFragment : Fragment() {

    lateinit var nav: NavigationView
    lateinit var toggle: ActionBarDrawerToggle
    lateinit var drawerLayout: DrawerLayout
    private var backToast :Toast? = null
    private var backPressedtime :Long = 0L

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view :View = inflater.inflate(R.layout.fragment_menu, container, false)

        nav = view.findViewById<NavigationView>(R.id.nav_view)
        drawerLayout = view.findViewById<DrawerLayout>(R.id.drawer)
        var View_Pager = view.findViewById<ViewPager>(R.id.view_pager)
        var Tab = view.findViewById<TabLayout>(R.id.tabs)

        var calender = view.findViewById<LinearLayout>(R.id.tvCalender)
        var image_calender = view.findViewById<ImageView>(R.id.ivCalender)
        var spinner = view.findViewById<MaterialSpinner>(R.id.spCalender)
        var Search_Edit = view.findViewById<EditText>(R.id.search_edit)
        var Search = view.findViewById<ImageView>(R.id.ivSearch)
        var Close = view.findViewById<ImageView>(R.id.ivClose)

        var mon = view.findViewById<LinearLayout>(R.id.mon)
        var tue = view.findViewById<LinearLayout>(R.id.tue)
        var wed = view.findViewById<LinearLayout>(R.id.wed)
        var thurs = view.findViewById<LinearLayout>(R.id.thurs)
        var fri = view.findViewById<LinearLayout>(R.id.fri)
        var sat = view.findViewById<LinearLayout>(R.id.sat)
        var sun = view.findViewById<LinearLayout>(R.id.sun)

        val toolbar = view.findViewById<View>(R.id.toolbar) as Toolbar

        toggle = ActionBarDrawerToggle(activity, drawerLayout,toolbar, R.string.open, R.string.close)
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()


        nav.setNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.Dashboard -> {
                    drawerLayout.closeDrawer(GravityCompat.START)
                }
                R.id.My_Account -> {
                    drawerLayout.closeDrawer(GravityCompat.START)
                   var action = MenuFragmentDirections.actionMenuFragmentToMyProfielFragment()
                    findNavController().navigate(action)

                }
                R.id.Notification -> {
                    drawerLayout.closeDrawer(GravityCompat.START)
                }
                R.id.Setting -> {
                    drawerLayout.closeDrawer(GravityCompat.START)
                    var action = MenuFragmentDirections.actionMenuFragmentToSettingFragment()
                    findNavController().navigate(action)

                }
                R.id.Legal -> {
                    drawerLayout.closeDrawer(GravityCompat.START)
                    var action = MenuFragmentDirections.actionMenuFragmentToLegalFragment()
                    findNavController().navigate(action)
                }
                R.id.Share_app -> {
                    drawerLayout.closeDrawer(GravityCompat.START)
                }
                R.id.Rate_app -> {
                    drawerLayout.closeDrawer(GravityCompat.START)
                }
                R.id.Logout -> {
                    drawerLayout.closeDrawer(GravityCompat.START)
                    var action = MenuFragmentDirections.actionMenuFragmentToLogInFragment()
                    findNavController().navigate(action)
                    Toast.makeText(activity,"LogOut Successfully",Toast.LENGTH_SHORT).show()
                }
            }
            true
        }


        var edit = view.findViewById<ImageView>(R.id.Edit_Profile)

        edit.setOnClickListener {
            var action = MenuFragmentDirections.actionMenuFragmentToEditProfileFragment()
            findNavController().navigate(action)
        }

        calender.visibility = View.GONE

        image_calender.setOnClickListener{
            image_calender.setColorFilter(resources.getColor(com.example.scout.R.color.green))
            calender.visibility = View.VISIBLE

            val SPINNER_DATA = arrayOf("Month", "Month to date", "Last Month", "Year to date" , "Custom")

            val adapter: ArrayAdapter<String>? = this!!.activity?.let { it1 ->
                ArrayAdapter<String>(
                    it1,
                    android.R.layout.simple_dropdown_item_1line,
                    SPINNER_DATA
                )
            }

            if (adapter != null) {
                spinner.setAdapter(adapter)
            }
        }

        Search_Edit.visibility = View.GONE
        Close.visibility = View.GONE

        Search.setOnClickListener {

            Search_Edit.visibility = View.VISIBLE
            Search.visibility = View.GONE
            Close.visibility = View.VISIBLE
        }

        Close.setOnClickListener {
            Search_Edit.visibility = View.GONE
            Search.visibility = View.VISIBLE
            Close.visibility = View.GONE
        }


        val adapter = MyViewPagerAdapter(childFragmentManager)
        adapter.addFragment(HomePageFragment() , " All Provider ")
        adapter.addFragment(HomePageFragment() , " Dr.John ")
        adapter.addFragment(HomePageFragment() , " Dr.Smith ")
        adapter.addFragment(HomePageFragment() , "Dr.Williab ")
        View_Pager.adapter = adapter
        Tab.setupWithViewPager(View_Pager)

        mon.setOnClickListener {
            mon.setBackgroundResource(com.example.scout.R.drawable.round_border);
            tue.setBackgroundResource(com.example.scout.R.color.calender_back);
            wed.setBackgroundResource(com.example.scout.R.color.calender_back);
            thurs.setBackgroundResource(com.example.scout.R.color.calender_back);
            fri.setBackgroundResource(com.example.scout.R.color.calender_back);
            sat.setBackgroundResource(com.example.scout.R.color.calender_back);
            sun.setBackgroundResource(com.example.scout.R.color.calender_back);
        }

        tue.setOnClickListener {
            mon.setBackgroundResource(com.example.scout.R.color.calender_back);
            tue.setBackgroundResource(com.example.scout.R.drawable.round_border);
            wed.setBackgroundResource(com.example.scout.R.color.calender_back);
            thurs.setBackgroundResource(com.example.scout.R.color.calender_back);
            fri.setBackgroundResource(com.example.scout.R.color.calender_back);
            sat.setBackgroundResource(com.example.scout.R.color.calender_back);
            sun.setBackgroundResource(com.example.scout.R.color.calender_back);
        }

        wed.setOnClickListener {
            mon.setBackgroundResource(com.example.scout.R.color.calender_back);
            tue.setBackgroundResource(com.example.scout.R.color.calender_back);
            wed.setBackgroundResource(com.example.scout.R.drawable.round_border);
            thurs.setBackgroundResource(com.example.scout.R.color.calender_back);
            fri.setBackgroundResource(com.example.scout.R.color.calender_back);
            sat.setBackgroundResource(com.example.scout.R.color.calender_back);
            sun.setBackgroundResource(com.example.scout.R.color.calender_back);
        }

        thurs.setOnClickListener {
            mon.setBackgroundResource(com.example.scout.R.color.calender_back);
            tue.setBackgroundResource(com.example.scout.R.color.calender_back);
            wed.setBackgroundResource(com.example.scout.R.color.calender_back);
            thurs.setBackgroundResource(com.example.scout.R.drawable.round_border);
            fri.setBackgroundResource(com.example.scout.R.color.calender_back);
            sat.setBackgroundResource(com.example.scout.R.color.calender_back);
            sun.setBackgroundResource(com.example.scout.R.color.calender_back);
        }

        fri.setOnClickListener {
            mon.setBackgroundResource(com.example.scout.R.color.calender_back);
            tue.setBackgroundResource(com.example.scout.R.color.calender_back);
            wed.setBackgroundResource(com.example.scout.R.color.calender_back);
            thurs.setBackgroundResource(com.example.scout.R.color.calender_back);
            fri.setBackgroundResource(com.example.scout.R.drawable.round_border);
            sat.setBackgroundResource(com.example.scout.R.color.calender_back);
            sun.setBackgroundResource(com.example.scout.R.color.calender_back);
        }

        sat.setOnClickListener {
            mon.setBackgroundResource(com.example.scout.R.color.calender_back);
            tue.setBackgroundResource(com.example.scout.R.color.calender_back);
            wed.setBackgroundResource(com.example.scout.R.color.calender_back);
            thurs.setBackgroundResource(com.example.scout.R.color.calender_back);
            fri.setBackgroundResource(com.example.scout.R.color.calender_back);
            sat.setBackgroundResource(com.example.scout.R.drawable.round_border);
            sun.setBackgroundResource(com.example.scout.R.color.calender_back);
        }

        sun.setOnClickListener {
            mon.setBackgroundResource(com.example.scout.R.color.calender_back);
            tue.setBackgroundResource(com.example.scout.R.color.calender_back);
            wed.setBackgroundResource(com.example.scout.R.color.calender_back);
            thurs.setBackgroundResource(com.example.scout.R.color.calender_back);
            fri.setBackgroundResource(com.example.scout.R.color.calender_back);
            sat.setBackgroundResource(com.example.scout.R.color.calender_back);
            sun.setBackgroundResource(com.example.scout.R.drawable.round_border);
        }


        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                val a = Intent(Intent.ACTION_MAIN)
                a.addCategory(Intent.CATEGORY_HOME)
                a.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(a)
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner,callback)


        return view
    }

//    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
//        inflater.inflate(R.menu.icon_search, menu)
//        val searchItem = menu.findItem(R.id.menuSearch)
//        var searchview = searchItem?.actionView as SearchView
//        super.onCreateOptionsMenu(menu, inflater)
//
//        searchview.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
//            override fun onQueryTextSubmit(p0: String?): Boolean {
//                searchview.clearFocus()
//                searchview.setQuery("",false)
//                searchItem.collapseActionView()
//                return true
//            }
//
//            override fun onQueryTextChange(p0: String?): Boolean {
//                return false
//            }
//        })
//    }

    class MyViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {

        private val fragmentList: MutableList<Fragment> = ArrayList()
        private val titleList: MutableList<String> = ArrayList()

        override fun getItem(position: Int): Fragment {
            return fragmentList[position]
        }

        override fun getCount(): Int {
            return fragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String) {
            fragmentList.add(fragment)
            titleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return titleList[position]
        }


    }
}