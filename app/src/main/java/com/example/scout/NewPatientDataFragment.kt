package com.example.scout

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.fragment_hygiene__goal.view.*

class NewPatientDataFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view :View = inflater.inflate(R.layout.fragment_new_patient_data, container, false)

        var new_patient_goal = view.findViewById<ProgressBar>(R.id.new_Patient_goal)
        var new_patient_comparison = view.findViewById<ProgressBar>(R.id.new_Patient_Comparison)
        var production_per_patient_goal = view.findViewById<ProgressBar>(R.id.production_per_patient_goal)
        var production_per_patient_comparison = view.findViewById<ProgressBar>(R.id.production_per_patient_comparison)

        new_patient_goal.setOnClickListener {

            var dialogview = LayoutInflater.from(activity).inflate(R.layout.fragment_new__patient__goal,null)
            var builder = activity?.let { it1 -> AlertDialog.Builder(it1,R.style.CustomAlertDialog) }
            builder!!.setView(dialogview)
            var alertDialog = builder.show()

            dialogview.ivclose.setOnClickListener {
                alertDialog.dismiss()
            }
        }

        new_patient_comparison.setOnClickListener {

            var dialogview = LayoutInflater.from(activity).inflate(R.layout.fragment_new__patient__comparison,null)
            var builder = activity?.let { it1 -> AlertDialog.Builder(it1,R.style.CustomAlertDialog) }
            builder!!.setView(dialogview)
            var alertDialog = builder.show()

            dialogview.ivclose.setOnClickListener {
                alertDialog.dismiss()
            }
        }

        production_per_patient_goal.setOnClickListener {

            var dialogview = LayoutInflater.from(activity).inflate(R.layout.fragment_production__per__patient__goal,null)
            var builder = activity?.let { it1 -> AlertDialog.Builder(it1,R.style.CustomAlertDialog) }
            builder!!.setView(dialogview)
            var alertDialog = builder.show()

            dialogview.ivclose.setOnClickListener {
                alertDialog.dismiss()
            }
        }

        production_per_patient_comparison.setOnClickListener {

            var dialogview = LayoutInflater.from(activity).inflate(R.layout.fragment_production__per__patient__comparison,null)
            var builder = activity?.let { it1 -> AlertDialog.Builder(it1,R.style.CustomAlertDialog) }
            builder!!.setView(dialogview)
            var alertDialog = builder.show()

            dialogview.ivclose.setOnClickListener {
                alertDialog.dismiss()
            }
        }

        return view
    }
}