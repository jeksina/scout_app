package com.example.scout

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.navigation.fragment.findNavController
import com.example.scout.Data.UserDatabase
import kotlinx.android.synthetic.main.fragment_forgot__password.*

class Forgot_PasswordFragment : Fragment() {

    private lateinit var userDatabse : UserDatabase

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view :View = inflater.inflate(R.layout.fragment_forgot__password, container, false)

        var Back = view.findViewById<ImageView>(R.id.ivForgot_back)
        var Next = view.findViewById<Button>(R.id.btnForgot_Next)

        Back.setOnClickListener {
            var action = Forgot_PasswordFragmentDirections.actionForgotPasswordFragmentToLogInFragment()
            findNavController().navigate(action)
        }

        Next.setOnClickListener {

            insetData()
        }

        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                var action = Forgot_PasswordFragmentDirections.actionForgotPasswordFragmentToLogInFragment()
                findNavController().navigate(action)
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner,callback)


        return view
    }

    private fun insetData() {
        var emailAddress = etForgot_Email.text.toString()
        var phoneNo = etForgot_Number.text.toString()

        if (emailAddress.isEmpty() || phoneNo.isEmpty()){
            Toast.makeText(activity,"Fill all th Fields!!", Toast.LENGTH_SHORT).show()
        }else{
            userDatabse = activity?.let { UserDatabase.getDatabase(it) }!!
            var userDao = userDatabse.userDao()

            Thread(Runnable {
                var userEntity = userDao.Forgot_Password(emailAddress,phoneNo)
                if (userEntity == null){
                    requireActivity().runOnUiThread(kotlinx.coroutines.Runnable {

                        Toast.makeText(activity,"InValid Email Address Or Phone Number", Toast.LENGTH_SHORT).show()

                    })
                }else{
                    var action = Forgot_PasswordFragmentDirections.actionForgotPasswordFragmentToNewPasswordFragment(userEntity)
                    findNavController().navigate(action)
                }
            }).start()
        }

    }


}