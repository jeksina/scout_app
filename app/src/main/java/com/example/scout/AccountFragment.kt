package com.example.scout

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.navigation.fragment.findNavController
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.jaredrummler.materialspinner.MaterialSpinner

class AccountFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view :View = inflater.inflate(R.layout.fragment_account, container, false)

        var Account_calender = view.findViewById<LinearLayout>(R.id.tvAccount_Calender)
        var Accountimage_calender = view.findViewById<ImageView>(R.id.ivAccount_Calender)
        var Account_spinner = view.findViewById<MaterialSpinner>(R.id.spAccount_Calender)

        var View_Pager = view.findViewById<ViewPager>(R.id.account_viewPager)
        var Tab = view.findViewById<TabLayout>(R.id.account_tabs)

        var Account_mon = view.findViewById<LinearLayout>(R.id.Account_mon)
        var Account_tue = view.findViewById<LinearLayout>(R.id.Account_tue)
        var Account_wed = view.findViewById<LinearLayout>(R.id.Account_wed)
        var Account_thurs = view.findViewById<LinearLayout>(R.id.Account_thurs)
        var Account_fri = view.findViewById<LinearLayout>(R.id.Account_fri)
        var Account_sat = view.findViewById<LinearLayout>(R.id.Account_sat)
        var Account_sun = view.findViewById<LinearLayout>(R.id.Account_sun)

        var Back = view.findViewById<ImageView>(R.id.Account_Back)

        Back.setOnClickListener {

            var action = AccountFragmentDirections.actionAccountFragmentToMenuFragment()
            findNavController().navigate(action)
        }


        Account_calender.visibility = View.GONE

        Accountimage_calender.setOnClickListener{
            Accountimage_calender.setColorFilter(resources.getColor(com.example.scout.R.color.green))
            Account_calender.visibility = View.VISIBLE

            val SPINNER_DATA = arrayOf("Month", "Month to date", "Last Month", "Year to date" , "Custom")

            val adapter: ArrayAdapter<String>? = this!!.activity?.let { it1 ->
                ArrayAdapter<String>(
                    it1,
                    android.R.layout.simple_dropdown_item_1line,
                    SPINNER_DATA
                )
            }

            if (adapter != null) {
                Account_spinner.setAdapter(adapter)
            }
        }

        val adapter = MyViewPagerAdapter(childFragmentManager)
        adapter.addFragment(AccountDataFragment() , " All Provider ")
        adapter.addFragment(AccountDataFragment() , " Dr.John ")
        adapter.addFragment(AccountDataFragment() , " Dr.Smith ")
        adapter.addFragment(AccountDataFragment() , "Dr.Williab ")
        View_Pager.adapter = adapter
        Tab.setupWithViewPager(View_Pager)

        Account_mon.setOnClickListener {
            Account_mon.setBackgroundResource(com.example.scout.R.drawable.round_border);
            Account_tue.setBackgroundResource(com.example.scout.R.color.calender_back);
            Account_wed.setBackgroundResource(com.example.scout.R.color.calender_back);
            Account_thurs.setBackgroundResource(com.example.scout.R.color.calender_back);
            Account_fri.setBackgroundResource(com.example.scout.R.color.calender_back);
            Account_sat.setBackgroundResource(com.example.scout.R.color.calender_back);
            Account_sun.setBackgroundResource(com.example.scout.R.color.calender_back);
        }

        Account_tue.setOnClickListener {
            Account_mon.setBackgroundResource(com.example.scout.R.color.calender_back);
            Account_tue.setBackgroundResource(com.example.scout.R.drawable.round_border);
            Account_wed.setBackgroundResource(com.example.scout.R.color.calender_back);
            Account_thurs.setBackgroundResource(com.example.scout.R.color.calender_back);
            Account_fri.setBackgroundResource(com.example.scout.R.color.calender_back);
            Account_sat.setBackgroundResource(com.example.scout.R.color.calender_back);
            Account_sun.setBackgroundResource(com.example.scout.R.color.calender_back);
        }

        Account_wed.setOnClickListener {
            Account_mon.setBackgroundResource(com.example.scout.R.color.calender_back);
            Account_tue.setBackgroundResource(com.example.scout.R.color.calender_back);
            Account_wed.setBackgroundResource(com.example.scout.R.drawable.round_border);
            Account_thurs.setBackgroundResource(com.example.scout.R.color.calender_back);
            Account_fri.setBackgroundResource(com.example.scout.R.color.calender_back);
            Account_sat.setBackgroundResource(com.example.scout.R.color.calender_back);
            Account_sun.setBackgroundResource(com.example.scout.R.color.calender_back);
        }

        Account_thurs.setOnClickListener {
            Account_mon.setBackgroundResource(com.example.scout.R.color.calender_back);
            Account_tue.setBackgroundResource(com.example.scout.R.color.calender_back);
            Account_wed.setBackgroundResource(com.example.scout.R.color.calender_back);
            Account_thurs.setBackgroundResource(com.example.scout.R.drawable.round_border);
            Account_fri.setBackgroundResource(com.example.scout.R.color.calender_back);
            Account_sat.setBackgroundResource(com.example.scout.R.color.calender_back);
            Account_sun.setBackgroundResource(com.example.scout.R.color.calender_back);
        }

        Account_fri.setOnClickListener {
            Account_mon.setBackgroundResource(com.example.scout.R.color.calender_back);
            Account_tue.setBackgroundResource(com.example.scout.R.color.calender_back);
            Account_wed.setBackgroundResource(com.example.scout.R.color.calender_back);
            Account_thurs.setBackgroundResource(com.example.scout.R.color.calender_back);
            Account_fri.setBackgroundResource(com.example.scout.R.drawable.round_border);
            Account_sat.setBackgroundResource(com.example.scout.R.color.calender_back);
            Account_sun.setBackgroundResource(com.example.scout.R.color.calender_back);
        }

        Account_sat.setOnClickListener {
            Account_mon.setBackgroundResource(com.example.scout.R.color.calender_back);
            Account_tue.setBackgroundResource(com.example.scout.R.color.calender_back);
            Account_wed.setBackgroundResource(com.example.scout.R.color.calender_back);
            Account_thurs.setBackgroundResource(com.example.scout.R.color.calender_back);
            Account_fri.setBackgroundResource(com.example.scout.R.color.calender_back);
            Account_sat.setBackgroundResource(com.example.scout.R.drawable.round_border);
            Account_sun.setBackgroundResource(com.example.scout.R.color.calender_back);
        }

        Account_sun.setOnClickListener {
            Account_mon.setBackgroundResource(com.example.scout.R.color.calender_back);
            Account_tue.setBackgroundResource(com.example.scout.R.color.calender_back);
            Account_wed.setBackgroundResource(com.example.scout.R.color.calender_back);
            Account_thurs.setBackgroundResource(com.example.scout.R.color.calender_back);
            Account_fri.setBackgroundResource(com.example.scout.R.color.calender_back);
            Account_sat.setBackgroundResource(com.example.scout.R.color.calender_back);
            Account_sun.setBackgroundResource(com.example.scout.R.drawable.round_border);
        }

        return view
    }

    class MyViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {

        private val fragmentList: MutableList<Fragment> = ArrayList()
        private val titleList: MutableList<String> = ArrayList()

        override fun getItem(position: Int): Fragment {
            return fragmentList[position]
        }

        override fun getCount(): Int {
            return fragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String) {
            fragmentList.add(fragment)
            titleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return titleList[position]
        }
    }
}