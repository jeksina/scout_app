package com.example.scout

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.navigation.fragment.findNavController
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.jaredrummler.materialspinner.MaterialSpinner

class HygieneFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view :View = inflater.inflate(R.layout.fragment_hygiene, container, false)

        var Hygiene_calender = view.findViewById<LinearLayout>(R.id.tvHygiene_Calender)
        var Hygieneimage_calender = view.findViewById<ImageView>(R.id.ivHygiene_Calender)
        var Hygiene_spinner = view.findViewById<MaterialSpinner>(R.id.spHygiene_Calender)

        var View_Pager = view.findViewById<ViewPager>(R.id.Hygiene_viewPager)
        var Tab = view.findViewById<TabLayout>(R.id.Hygiene_tabs)

        var Hygiene_mon = view.findViewById<LinearLayout>(R.id.Hygiene_mon)
        var Hygiene_tue = view.findViewById<LinearLayout>(R.id.Hygiene_tue)
        var Hygiene_wed = view.findViewById<LinearLayout>(R.id.Hygiene_wed)
        var Hygiene_thurs = view.findViewById<LinearLayout>(R.id.Hygiene_thurs)
        var Hygiene_fri = view.findViewById<LinearLayout>(R.id.Hygiene_fri)
        var Hygiene_sat = view.findViewById<LinearLayout>(R.id.Hygiene_sat)
        var Hygiene_sun = view.findViewById<LinearLayout>(R.id.Hygiene_sun)

        var Back = view.findViewById<ImageView>(R.id.Hygiene_Back)

        Back.setOnClickListener {

            var action = HygieneFragmentDirections.actionHygieneFragmentToMenuFragment()
            findNavController().navigate(action)
        }


        Hygiene_calender.visibility = View.GONE

        Hygieneimage_calender.setOnClickListener{
            Hygieneimage_calender.setColorFilter(resources.getColor(com.example.scout.R.color.green))
            Hygiene_calender.visibility = View.VISIBLE

            val SPINNER_DATA = arrayOf("Month", "Month to date", "Last Month", "Year to date" , "Custom")

            val adapter: ArrayAdapter<String>? = this!!.activity?.let { it1 ->
                ArrayAdapter<String>(
                    it1,
                    android.R.layout.simple_dropdown_item_1line,
                    SPINNER_DATA
                )
            }

            if (adapter != null) {
                Hygiene_spinner.setAdapter(adapter)
            }
        }

        val adapter = MyViewPagerAdapter(childFragmentManager)
        adapter.addFragment(HygieneDataFragment() , " All Provider ")
        adapter.addFragment(HygieneDataFragment() , " Dr.John ")
        adapter.addFragment(HygieneDataFragment() , " Dr.Smith ")
        adapter.addFragment(HygieneDataFragment() , "Dr.Williab ")
        View_Pager.adapter = adapter
        Tab.setupWithViewPager(View_Pager)

        Hygiene_mon.setOnClickListener {
            Hygiene_mon.setBackgroundResource(com.example.scout.R.drawable.round_border);
            Hygiene_tue.setBackgroundResource(com.example.scout.R.color.calender_back);
            Hygiene_wed.setBackgroundResource(com.example.scout.R.color.calender_back);
            Hygiene_thurs.setBackgroundResource(com.example.scout.R.color.calender_back);
            Hygiene_fri.setBackgroundResource(com.example.scout.R.color.calender_back);
            Hygiene_sat.setBackgroundResource(com.example.scout.R.color.calender_back);
            Hygiene_sun.setBackgroundResource(com.example.scout.R.color.calender_back);
        }

        Hygiene_tue.setOnClickListener {
            Hygiene_mon.setBackgroundResource(com.example.scout.R.color.calender_back);
            Hygiene_tue.setBackgroundResource(com.example.scout.R.drawable.round_border);
            Hygiene_wed.setBackgroundResource(com.example.scout.R.color.calender_back);
            Hygiene_thurs.setBackgroundResource(com.example.scout.R.color.calender_back);
            Hygiene_fri.setBackgroundResource(com.example.scout.R.color.calender_back);
            Hygiene_sat.setBackgroundResource(com.example.scout.R.color.calender_back);
            Hygiene_sun.setBackgroundResource(com.example.scout.R.color.calender_back);
        }

        Hygiene_wed.setOnClickListener {
            Hygiene_mon.setBackgroundResource(com.example.scout.R.color.calender_back);
            Hygiene_tue.setBackgroundResource(com.example.scout.R.color.calender_back);
            Hygiene_wed.setBackgroundResource(com.example.scout.R.drawable.round_border);
            Hygiene_thurs.setBackgroundResource(com.example.scout.R.color.calender_back);
            Hygiene_fri.setBackgroundResource(com.example.scout.R.color.calender_back);
            Hygiene_sat.setBackgroundResource(com.example.scout.R.color.calender_back);
            Hygiene_sun.setBackgroundResource(com.example.scout.R.color.calender_back);
        }

        Hygiene_thurs.setOnClickListener {
            Hygiene_mon.setBackgroundResource(com.example.scout.R.color.calender_back);
            Hygiene_tue.setBackgroundResource(com.example.scout.R.color.calender_back);
            Hygiene_wed.setBackgroundResource(com.example.scout.R.color.calender_back);
            Hygiene_thurs.setBackgroundResource(com.example.scout.R.drawable.round_border);
            Hygiene_fri.setBackgroundResource(com.example.scout.R.color.calender_back);
            Hygiene_sat.setBackgroundResource(com.example.scout.R.color.calender_back);
            Hygiene_sun.setBackgroundResource(com.example.scout.R.color.calender_back);
        }

        Hygiene_fri.setOnClickListener {
            Hygiene_mon.setBackgroundResource(com.example.scout.R.color.calender_back);
            Hygiene_tue.setBackgroundResource(com.example.scout.R.color.calender_back);
            Hygiene_wed.setBackgroundResource(com.example.scout.R.color.calender_back);
            Hygiene_thurs.setBackgroundResource(com.example.scout.R.color.calender_back);
            Hygiene_fri.setBackgroundResource(com.example.scout.R.drawable.round_border);
            Hygiene_sat.setBackgroundResource(com.example.scout.R.color.calender_back);
            Hygiene_sun.setBackgroundResource(com.example.scout.R.color.calender_back);
        }

        Hygiene_sat.setOnClickListener {
            Hygiene_mon.setBackgroundResource(com.example.scout.R.color.calender_back);
            Hygiene_tue.setBackgroundResource(com.example.scout.R.color.calender_back);
            Hygiene_wed.setBackgroundResource(com.example.scout.R.color.calender_back);
            Hygiene_thurs.setBackgroundResource(com.example.scout.R.color.calender_back);
            Hygiene_fri.setBackgroundResource(com.example.scout.R.color.calender_back);
            Hygiene_sat.setBackgroundResource(com.example.scout.R.drawable.round_border);
            Hygiene_sun.setBackgroundResource(com.example.scout.R.color.calender_back);
        }

        Hygiene_sun.setOnClickListener {
            Hygiene_mon.setBackgroundResource(com.example.scout.R.color.calender_back);
            Hygiene_tue.setBackgroundResource(com.example.scout.R.color.calender_back);
            Hygiene_wed.setBackgroundResource(com.example.scout.R.color.calender_back);
            Hygiene_thurs.setBackgroundResource(com.example.scout.R.color.calender_back);
            Hygiene_fri.setBackgroundResource(com.example.scout.R.color.calender_back);
            Hygiene_sat.setBackgroundResource(com.example.scout.R.color.calender_back);
            Hygiene_sun.setBackgroundResource(com.example.scout.R.drawable.round_border);
        }

        return view
    }

    class MyViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {

        private val fragmentList: MutableList<Fragment> = ArrayList()
        private val titleList: MutableList<String> = ArrayList()

        override fun getItem(position: Int): Fragment {
            return fragmentList[position]
        }

        override fun getCount(): Int {
            return fragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String) {
            fragmentList.add(fragment)
            titleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return titleList[position]
        }
    }
}