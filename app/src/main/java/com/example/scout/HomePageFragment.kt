package com.example.scout

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.navigation.fragment.findNavController

class HomePageFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view :View = inflater.inflate(R.layout.fragment_home_page, container, false)

        var production = view.findViewById<LinearLayout>(R.id.lProduction)
        var collection = view.findViewById<LinearLayout>(R.id.lCollection)
        var Patient = view.findViewById<LinearLayout>(R.id.lNew_Patients)
        var Hygiene = view.findViewById<LinearLayout>(R.id.lHygiene)
        var Procedure = view.findViewById<LinearLayout>(R.id.lProcedures)
        var Case_acceptance = view.findViewById<LinearLayout>(R.id.lCase_Acceptance)
        var Account = view.findViewById<LinearLayout>(R.id.laccount)

        production.setOnClickListener {

            var action = MenuFragmentDirections.actionMenuFragmentToProductionFragment()
            findNavController().navigate(action)
        }

        collection.setOnClickListener {

            var action = MenuFragmentDirections.actionMenuFragmentToCollectionFragment()
            findNavController().navigate(action)
        }

        Patient.setOnClickListener {

            var action = MenuFragmentDirections.actionMenuFragmentToNewPatientFragment()
            findNavController().navigate(action)
        }

        Hygiene.setOnClickListener {

            var action = MenuFragmentDirections.actionMenuFragmentToHygieneFragment()
            findNavController().navigate(action)
        }

        Procedure.setOnClickListener {

            var action = MenuFragmentDirections.actionMenuFragmentToProcedureFragment()
            findNavController().navigate(action)
        }

        Case_acceptance.setOnClickListener {

            var action = MenuFragmentDirections.actionMenuFragmentToCaseAcceptanceFragment()
            findNavController().navigate(action)
        }

        Account.setOnClickListener {

            var action = MenuFragmentDirections.actionMenuFragmentToAccountFragment()
            findNavController().navigate(action)
        }

        return view
    }
}