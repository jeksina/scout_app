package com.example.scout

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.activity.OnBackPressedCallback
import androidx.navigation.fragment.findNavController

class Term_ConditionFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view :View = inflater.inflate(R.layout.fragment_term__condition, container, false)

        var Back_Arrow = view.findViewById<ImageView>(R.id.ivTerm_backarrow)

        Back_Arrow.setOnClickListener {

            var action = Term_ConditionFragmentDirections.actionTermConditionFragmentToLegalFragment()
            findNavController().navigate(action)
        }

        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                var action = Term_ConditionFragmentDirections.actionTermConditionFragmentToLegalFragment()
                findNavController().navigate(action)
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner,callback)

        return view
    }
}