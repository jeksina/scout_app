package com.example.scout.Case_Acceptance_Fragmnet

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.navigation.fragment.findNavController
import androidx.viewpager.widget.ViewPager
import com.example.scout.CaseAcceptanceFragmentDirections
import com.example.scout.R
import com.google.android.material.tabs.TabLayout
import com.jaredrummler.materialspinner.MaterialSpinner

class CaseAcceptanceFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        var view :View = inflater.inflate(R.layout.fragment_case_acceptance, container, false)

        var Case_Acceptance_calender = view.findViewById<LinearLayout>(R.id.tvCase_Calender)
        var Case_Acceptanceimage_calender = view.findViewById<ImageView>(R.id.ivCase_Calender)
        var Case_Acceptancespinner = view.findViewById<MaterialSpinner>(R.id.spCase_Calender)

        var View_Pager = view.findViewById<ViewPager>(R.id.Case_acceptance_viewPager)
        var Tab = view.findViewById<TabLayout>(R.id.Case_acceptance_tabs)

        var Case_mon = view.findViewById<LinearLayout>(R.id.Case_mon)
        var Case_tue = view.findViewById<LinearLayout>(R.id.Case_tue)
        var Case_wed = view.findViewById<LinearLayout>(R.id.Case_wed)
        var Case_thurs = view.findViewById<LinearLayout>(R.id.Case_thurs)
        var Case_fri = view.findViewById<LinearLayout>(R.id.Case_fri)
        var Case_sat = view.findViewById<LinearLayout>(R.id.Case_sat)
        var Case_sun = view.findViewById<LinearLayout>(R.id.Case_sun)

        var Back = view.findViewById<ImageView>(R.id.Case_Acceptance_Back)

        Back.setOnClickListener {

            var action =
                CaseAcceptanceFragmentDirections.actionCaseAcceptanceFragmentToMenuFragment()
            findNavController().navigate(action)
        }


        Case_Acceptance_calender.visibility = View.GONE

        Case_Acceptanceimage_calender.setOnClickListener{
            Case_Acceptanceimage_calender.setColorFilter(resources.getColor(R.color.green))
            Case_Acceptance_calender.visibility = View.VISIBLE

            val SPINNER_DATA = arrayOf("Month", "Month to date", "Last Month", "Year to date" , "Custom")

            val adapter: ArrayAdapter<String>? = this!!.activity?.let { it1 ->
                ArrayAdapter<String>(
                    it1,
                    android.R.layout.simple_dropdown_item_1line,
                    SPINNER_DATA
                )
            }

            if (adapter != null) {
                Case_Acceptancespinner.setAdapter(adapter)
            }
        }

        val adapter =
            MyViewPagerAdapter(
                childFragmentManager
            )
        adapter.addFragment(CaseAcceptanceDataFragment(), " All Provider ")
        adapter.addFragment(CaseAcceptanceDataFragment(), " Dr.John ")
        adapter.addFragment(CaseAcceptanceDataFragment(), " Dr.Smith ")
        adapter.addFragment(CaseAcceptanceDataFragment(), "Dr.Williab ")
        View_Pager.adapter = adapter
        Tab.setupWithViewPager(View_Pager)

        Case_mon.setOnClickListener {
            Case_mon.setBackgroundResource(R.drawable.round_border);
            Case_tue.setBackgroundResource(R.color.calender_back);
            Case_wed.setBackgroundResource(R.color.calender_back);
            Case_thurs.setBackgroundResource(R.color.calender_back);
            Case_fri.setBackgroundResource(R.color.calender_back);
            Case_sat.setBackgroundResource(R.color.calender_back);
            Case_sun.setBackgroundResource(R.color.calender_back);
        }

        Case_tue.setOnClickListener {
            Case_mon.setBackgroundResource(R.color.calender_back);
            Case_tue.setBackgroundResource(R.drawable.round_border);
            Case_wed.setBackgroundResource(R.color.calender_back);
            Case_thurs.setBackgroundResource(R.color.calender_back);
            Case_fri.setBackgroundResource(R.color.calender_back);
            Case_sat.setBackgroundResource(R.color.calender_back);
            Case_sun.setBackgroundResource(R.color.calender_back);
        }

        Case_wed.setOnClickListener {
            Case_mon.setBackgroundResource(R.color.calender_back);
            Case_tue.setBackgroundResource(R.color.calender_back);
            Case_wed.setBackgroundResource(R.drawable.round_border);
            Case_thurs.setBackgroundResource(R.color.calender_back);
            Case_fri.setBackgroundResource(R.color.calender_back);
            Case_sat.setBackgroundResource(R.color.calender_back);
            Case_sun.setBackgroundResource(R.color.calender_back);
        }

        Case_thurs.setOnClickListener {
            Case_mon.setBackgroundResource(R.color.calender_back);
            Case_tue.setBackgroundResource(R.color.calender_back);
            Case_wed.setBackgroundResource(R.color.calender_back);
            Case_thurs.setBackgroundResource(R.drawable.round_border);
            Case_fri.setBackgroundResource(R.color.calender_back);
            Case_sat.setBackgroundResource(R.color.calender_back);
            Case_sun.setBackgroundResource(R.color.calender_back);
        }

        Case_fri.setOnClickListener {
            Case_mon.setBackgroundResource(R.color.calender_back);
            Case_tue.setBackgroundResource(R.color.calender_back);
            Case_wed.setBackgroundResource(R.color.calender_back);
            Case_thurs.setBackgroundResource(R.color.calender_back);
            Case_fri.setBackgroundResource(R.drawable.round_border);
            Case_sat.setBackgroundResource(R.color.calender_back);
            Case_sun.setBackgroundResource(R.color.calender_back);
        }

        Case_sat.setOnClickListener {
            Case_mon.setBackgroundResource(R.color.calender_back);
            Case_tue.setBackgroundResource(R.color.calender_back);
            Case_wed.setBackgroundResource(R.color.calender_back);
            Case_thurs.setBackgroundResource(R.color.calender_back);
            Case_fri.setBackgroundResource(R.color.calender_back);
            Case_sat.setBackgroundResource(R.drawable.round_border);
            Case_sun.setBackgroundResource(R.color.calender_back);
        }

        Case_sun.setOnClickListener {
            Case_mon.setBackgroundResource(R.color.calender_back);
            Case_tue.setBackgroundResource(R.color.calender_back);
            Case_wed.setBackgroundResource(R.color.calender_back);
            Case_thurs.setBackgroundResource(R.color.calender_back);
            Case_fri.setBackgroundResource(R.color.calender_back);
            Case_sat.setBackgroundResource(R.color.calender_back);
            Case_sun.setBackgroundResource(R.drawable.round_border);
        }


        return view
    }

    class MyViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {

        private val fragmentList: MutableList<Fragment> = ArrayList()
        private val titleList: MutableList<String> = ArrayList()

        override fun getItem(position: Int): Fragment {
            return fragmentList[position]
        }

        override fun getCount(): Int {
            return fragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String) {
            fragmentList.add(fragment)
            titleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return titleList[position]
        }
    }
}