package com.example.scout.Case_Acceptance_Fragmnet

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.appcompat.app.AlertDialog
import com.example.scout.R
import kotlinx.android.synthetic.main.fragment_hygiene__goal.view.*

class CaseAcceptanceDataFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view :View = inflater.inflate(R.layout.fragment_case_acceptance_data, container, false)

        var case_aceeptance_goal = view.findViewById<ProgressBar>(R.id.case_acceptance_goal)
        var case_aceeptance_comparison = view.findViewById<ProgressBar>(R.id.case_acceptance_Comparison)
        var diagnosis_goal = view.findViewById<ProgressBar>(R.id.diagnosis_goal)
        var diagnosis_comparison = view.findViewById<ProgressBar>(R.id.diagnosis_comparison)
        var case_patient_goal = view.findViewById<ProgressBar>(R.id.patient_acceptance_goal)
        var case_patient_comparison = view.findViewById<ProgressBar>(R.id.patient_acceptance_comparison)
        var treatment_goal = view.findViewById<ProgressBar>(R.id.treatment_goal)
        var treatment_comparison = view.findViewById<ProgressBar>(R.id.treatment_comparison)

        case_aceeptance_goal.setOnClickListener {

            var dialogview = LayoutInflater.from(activity).inflate(R.layout.fragment_case__acceptance__goal,null)
            var builder = activity?.let { it1 -> AlertDialog.Builder(it1,
                R.style.CustomAlertDialog
            ) }
            builder!!.setView(dialogview)
            var alertDialog = builder.show()

            dialogview.ivclose.setOnClickListener {
                alertDialog.dismiss()
            }
        }

        case_aceeptance_comparison.setOnClickListener {

            var dialogview = LayoutInflater.from(activity).inflate(R.layout.fragment_case_acceptance_coparison,null)
            var builder = activity?.let { it1 -> AlertDialog.Builder(it1,
                R.style.CustomAlertDialog
            ) }
            builder!!.setView(dialogview)
            var alertDialog = builder.show()

            dialogview.ivclose.setOnClickListener {
                alertDialog.dismiss()
            }
        }

        diagnosis_goal.setOnClickListener {

            var dialogview = LayoutInflater.from(activity).inflate(R.layout.fragment_diagosis__goal,null)
            var builder = activity?.let { it1 -> AlertDialog.Builder(it1,
                R.style.CustomAlertDialog
            ) }
            builder!!.setView(dialogview)
            var alertDialog = builder.show()

            dialogview.ivclose.setOnClickListener {
                alertDialog.dismiss()
            }
        }

        diagnosis_comparison.setOnClickListener {

            var dialogview = LayoutInflater.from(activity).inflate(R.layout.fragment_diagosis__comparison,null)
            var builder = activity?.let { it1 -> AlertDialog.Builder(it1,
                R.style.CustomAlertDialog
            ) }
            builder!!.setView(dialogview)
            var alertDialog = builder.show()

            dialogview.ivclose.setOnClickListener {
                alertDialog.dismiss()
            }
        }

        case_patient_goal.setOnClickListener {

            var dialogview = LayoutInflater.from(activity).inflate(R.layout.fragment_case__patient__goal,null)
            var builder = activity?.let { it1 -> AlertDialog.Builder(it1,
                R.style.CustomAlertDialog
            ) }
            builder!!.setView(dialogview)
            var alertDialog = builder.show()

            dialogview.ivclose.setOnClickListener {
                alertDialog.dismiss()
            }
        }

        case_patient_comparison.setOnClickListener {

            var dialogview = LayoutInflater.from(activity).inflate(R.layout.fragment_case__patient_comparison,null)
            var builder = activity?.let { it1 -> AlertDialog.Builder(it1,
                R.style.CustomAlertDialog
            ) }
            builder!!.setView(dialogview)
            var alertDialog = builder.show()

            dialogview.ivclose.setOnClickListener {
                alertDialog.dismiss()
            }
        }

        treatment_goal.setOnClickListener {

            var dialogview = LayoutInflater.from(activity).inflate(R.layout.fragment_treatment__goal,null)
            var builder = activity?.let { it1 -> AlertDialog.Builder(it1,
                R.style.CustomAlertDialog
            ) }
            builder!!.setView(dialogview)
            var alertDialog = builder.show()

            dialogview.ivclose.setOnClickListener {
                alertDialog.dismiss()
            }
        }

        treatment_comparison.setOnClickListener {

            var dialogview = LayoutInflater.from(activity).inflate(R.layout.fragment_treatment_comparison,null)
            var builder = activity?.let { it1 -> AlertDialog.Builder(it1,
                R.style.CustomAlertDialog
            ) }
            builder!!.setView(dialogview)
            var alertDialog = builder.show()

            dialogview.ivclose.setOnClickListener {
                alertDialog.dismiss()
            }
        }
        return view

    }
}