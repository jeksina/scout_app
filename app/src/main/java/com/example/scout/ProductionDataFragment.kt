package com.example.scout

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import androidx.appcompat.app.AlertDialog
import androidx.navigation.fragment.findNavController
import com.jaredrummler.materialspinner.MaterialSpinner
import kotlinx.android.synthetic.main.fragment_production__comparison.view.*
import kotlinx.android.synthetic.main.fragment_production__goal.view.*
import kotlinx.android.synthetic.main.fragment_production__hygiene__comparison.view.*
import kotlinx.android.synthetic.main.fragment_production__hygiene__goal.view.*
import kotlinx.android.synthetic.main.fragment_production__patient__comparison.view.*
import kotlinx.android.synthetic.main.fragment_production__patient__goal.view.*
import kotlinx.android.synthetic.main.fragment_production__same_day__comparison.view.*
import kotlinx.android.synthetic.main.fragment_production__same_day_goal.view.*

class ProductionDataFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view :View = inflater.inflate(R.layout.fragment_production_data, container, false)

        var production_goal = view.findViewById<ProgressBar>(R.id.production_goal)
        var production_comparison = view.findViewById<ProgressBar>(R.id.production_Comparison)
        var production_patient_goal = view.findViewById<ProgressBar>(R.id.production_patient_goal)
        var production_patient_comparison = view.findViewById<ProgressBar>(R.id.production_patient_comparison)
        var production_hygiene_goal = view.findViewById<ProgressBar>(R.id.production_hygiene_goal)
        var production_hygiene_comparison = view.findViewById<ProgressBar>(R.id.production_hygiene_comparison)
        var production_sameday_goal = view.findViewById<ProgressBar>(R.id.production_sameday_goal)
        var production_sameday_comparison = view.findViewById<ProgressBar>(R.id.production_sameday_comparison)

        production_goal.setOnClickListener {

            var dialogview = LayoutInflater.from(activity).inflate(R.layout.fragment_production__goal,null)
            var builder = activity?.let { it1 -> AlertDialog.Builder(it1,R.style.CustomAlertDialog) }
            builder!!.setView(dialogview)
            var alertDialog = builder.show()

            dialogview.Production_Goal_Close.setOnClickListener {
                alertDialog.dismiss()
            }
        }

        production_comparison.setOnClickListener {

            var dialogview = LayoutInflater.from(activity).inflate(R.layout.fragment_production__comparison,null)
            var builder = activity?.let { it1 -> AlertDialog.Builder(it1,R.style.CustomAlertDialog) }
            builder!!.setView(dialogview)
            var alertDialog = builder.show()

            dialogview.Production_Comparison_close.setOnClickListener {
                alertDialog.dismiss()
            }
        }

        production_patient_goal.setOnClickListener {

            var dialogview = LayoutInflater.from(activity).inflate(R.layout.fragment_production__patient__goal,null)
            var builder = activity?.let { it1 -> AlertDialog.Builder(it1,R.style.CustomAlertDialog) }
            builder!!.setView(dialogview)
            var alertDialog = builder.show()

            dialogview.production__patient__goal_close.setOnClickListener {
                alertDialog.dismiss()
            }
        }

        production_patient_comparison.setOnClickListener {

            var dialogview = LayoutInflater.from(activity).inflate(R.layout.fragment_production__patient__comparison,null)
            var builder = activity?.let { it1 -> AlertDialog.Builder(it1,R.style.CustomAlertDialog) }
            builder!!.setView(dialogview)
            var alertDialog = builder.show()

            dialogview.production__patient__comparison_close.setOnClickListener {
                alertDialog.dismiss()
            }
        }

        production_hygiene_goal.setOnClickListener {

            var dialogview = LayoutInflater.from(activity).inflate(R.layout.fragment_production__hygiene__goal,null)
            var builder = activity?.let { it1 -> AlertDialog.Builder(it1,R.style.CustomAlertDialog) }
            builder!!.setView(dialogview)
            var alertDialog = builder.show()

            dialogview.production__hygiene__goal_close.setOnClickListener {
                alertDialog.dismiss()
            }
        }

        production_hygiene_comparison.setOnClickListener {

            var dialogview = LayoutInflater.from(activity).inflate(R.layout.fragment_production__hygiene__comparison,null)
            var builder = activity?.let { it1 -> AlertDialog.Builder(it1,R.style.CustomAlertDialog) }
            builder!!.setView(dialogview)
            var alertDialog = builder.show()

            dialogview.production__hygiene__comparison_close.setOnClickListener {
                alertDialog.dismiss()
            }
        }

        production_sameday_goal.setOnClickListener {

            var dialogview = LayoutInflater.from(activity).inflate(R.layout.fragment_production__same_day_goal,null)
            var builder = activity?.let { it1 -> AlertDialog.Builder(it1,R.style.CustomAlertDialog) }
            builder!!.setView(dialogview)
            var alertDialog = builder.show()

            dialogview.iproduction__same_day_goal_close.setOnClickListener {
                alertDialog.dismiss()
            }
        }

        production_sameday_comparison.setOnClickListener {

            var dialogview = LayoutInflater.from(activity).inflate(R.layout.fragment_production__same_day__comparison,null)
            var builder = activity?.let { it1 -> AlertDialog.Builder(it1,R.style.CustomAlertDialog) }
            builder!!.setView(dialogview)
            var alertDialog = builder.show()

            dialogview.production__same_day__comparison_close.setOnClickListener {
                alertDialog.dismiss()
            }
        }

        return view
    }

}