package com.example.scout

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.scout.Data.UserDao
import com.example.scout.Data.UserDatabase
import kotlinx.android.synthetic.main.fragment_new_password.*

class NewPasswordFragment : Fragment() {

    private lateinit var userDatabse : UserDatabase
    private lateinit var uuserDao: UserDao
    private val args by navArgs<NewPasswordFragmentArgs>()



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view :View = inflater.inflate(R.layout.fragment_new_password, container, false)

        var Back = view.findViewById<ImageView>(R.id.ivNewPassword_back)
        var Next = view.findViewById<Button>(R.id.btnNewPassword_Next)

        Back.setOnClickListener {
            var action = NewPasswordFragmentDirections.actionNewPasswordFragmentToForgotPasswordFragment()
            findNavController().navigate(action)
        }

        Next.setOnClickListener {


            updateData()
        }

        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                var action = NewPasswordFragmentDirections.actionNewPasswordFragmentToForgotPasswordFragment()
                findNavController().navigate(action)
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner,callback)


        return view
    }


    private fun updateData() {
        var new_Password :String = etForgot_NewPassword.text.toString()

        if (new_Password.isEmpty()){
            Toast.makeText(activity,"Fill all th Fields!!",Toast.LENGTH_SHORT).show()
        }else{
            userDatabse = activity?.let { UserDatabase.getDatabase(it) }!!
            var userDao = userDatabse.userDao()

            Thread(Runnable {
                var userEntity = userDao.Password_Update(args.currentUser.id,new_Password)
                uuserDao.Password_Update(args.currentUser.id,new_Password)
                if (userEntity == null){
                    requireActivity().runOnUiThread(kotlinx.coroutines.Runnable {
                        Toast.makeText(activity,"InValid Email Address Or Password",Toast.LENGTH_SHORT).show()
                    })
                }else{
                    var action = NewPasswordFragmentDirections.actionNewPasswordFragmentToMenuFragment()
                    findNavController().navigate(action)

                }
            }).start()
        }

    }

}

