package com.example.scout

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.scout.Data.UserEntity
import com.example.scout.Data.UserViewModel
import kotlinx.android.synthetic.main.fragment_sign_up.*

class SignUpFragment : Fragment() {

   private lateinit var userViewModel :UserViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view :View = inflater.inflate(R.layout.fragment_sign_up, container, false)

        var LogIn = view.findViewById<TextView>(R.id.tvSignUpLogIn)
        var Location = view.findViewById<ImageView>(R.id.ivLocation)

        userViewModel = ViewModelProvider(this).get(UserViewModel::class.java)

        LogIn.setOnClickListener {

            var action = SignUpFragmentDirections.actionSignUpFragmentToLogInFragment()
            findNavController().navigate(action)
        }

        Location.setOnClickListener {

            var action = SignUpFragmentDirections.actionSignUpFragmentToMapFragment()
            findNavController().navigate(action)

            insertDataToDatabase()
        }


        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                val a = Intent(Intent.ACTION_MAIN)
                a.addCategory(Intent.CATEGORY_HOME)
                a.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(a)
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner,callback)

        return view
    }

    private fun insertDataToDatabase() {
        var firstName = etSignUp_fname.text.toString()
        var lname = etSignUp_lname.text.toString()
        var emailAddress = etSignUp_Upemail.text.toString()
        var phoneNo = etSignUp_phoneno.text.toString()
        var passworld = etSignUp_password.text.toString()
        var cPassword = etSignUp_cpassword.text.toString()
        var address = etSignUp_practiceadd.text.toString()

        if (inputCheck(firstName,lname,emailAddress,phoneNo,passworld,cPassword,address)){
            var userEntity = UserEntity(0,firstName,lname,emailAddress,phoneNo,passworld,cPassword,address)
            userViewModel.addUser(userEntity)
            }
    }

    private fun inputCheck(firstName:String, lastName:String, emailAddress:String, phoneNo: String, password:String, cPassword:String, address:String) : Boolean{
        return !(TextUtils.isEmpty(firstName) && TextUtils.isEmpty(lastName) && TextUtils.isEmpty(emailAddress) && phoneNo.isEmpty() && TextUtils.isEmpty(password)
                && TextUtils.isEmpty(cPassword) && TextUtils.isEmpty(address))
    }
}

