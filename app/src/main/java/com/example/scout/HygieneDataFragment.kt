package com.example.scout

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.fragment_hygiene__goal.view.*

class HygieneDataFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view :View = inflater.inflate(R.layout.fragment_hygiene_data, container, false)


        var hygiene_goal = view.findViewById<ProgressBar>(R.id.hygiene_goal)
        var hygiene_comparison = view.findViewById<ProgressBar>(R.id.hygiene_comparison)
        var hygiene_appointment_goal = view.findViewById<ProgressBar>(R.id.hygiene_appointment_goal)
        var hygiene_appointment_comparison = view.findViewById<ProgressBar>(R.id.hygiene_appointment_comparison)
        var hygiene_period_goal = view.findViewById<ProgressBar>(R.id.hygiene__period_goal)
        var hygiene_period_comparison = view.findViewById<ProgressBar>(R.id.hygiene__period_comparison)
        var hygiene_fluoride_goal = view.findViewById<ProgressBar>(R.id.hygiene_floride_goal)
        var hygiene_fluoride_comparison = view.findViewById<ProgressBar>(R.id.hygiene_floride_comparison)

        hygiene_goal.setOnClickListener {

            var dialogview = LayoutInflater.from(activity).inflate(R.layout.fragment_hygiene__goal,null)
            var builder = activity?.let { it1 -> AlertDialog.Builder(it1,R.style.CustomAlertDialog) }
            builder!!.setView(dialogview)
            var alertDialog = builder.show()

            dialogview.ivclose.setOnClickListener {
                alertDialog.dismiss()
            }
        }

        hygiene_comparison.setOnClickListener {

            var dialogview = LayoutInflater.from(activity).inflate(R.layout.fragment_hygiene__comparison,null)
            var builder = activity?.let { it1 -> AlertDialog.Builder(it1,R.style.CustomAlertDialog) }
            builder!!.setView(dialogview)
            var alertDialog = builder.show()

            dialogview.ivclose.setOnClickListener {
                alertDialog.dismiss()
            }
        }

        hygiene_appointment_goal.setOnClickListener {

            var dialogview = LayoutInflater.from(activity).inflate(R.layout.fragment_hygiene__appointment__goal,null)
            var builder = activity?.let { it1 -> AlertDialog.Builder(it1,R.style.CustomAlertDialog) }
            builder!!.setView(dialogview)
            var alertDialog = builder.show()

            dialogview.ivclose.setOnClickListener {
                alertDialog.dismiss()
            }
        }

        hygiene_appointment_comparison.setOnClickListener {

            var dialogview = LayoutInflater.from(activity).inflate(R.layout.fragment_hygiene_appointment__comparison,null)
            var builder = activity?.let { it1 -> AlertDialog.Builder(it1,R.style.CustomAlertDialog) }
            builder!!.setView(dialogview)
            var alertDialog = builder.show()

            dialogview.ivclose.setOnClickListener {
                alertDialog.dismiss()
            }
        }

        hygiene_period_goal.setOnClickListener {

            var dialogview = LayoutInflater.from(activity).inflate(R.layout.fragment_hygiene__period__goal,null)
            var builder = activity?.let { it1 -> AlertDialog.Builder(it1,R.style.CustomAlertDialog) }
            builder!!.setView(dialogview)
            var alertDialog = builder.show()

            dialogview.ivclose.setOnClickListener {
                alertDialog.dismiss()
            }
        }

        hygiene_period_comparison.setOnClickListener {

            var dialogview = LayoutInflater.from(activity).inflate(R.layout.fragment_hygiene__period__comparison,null)
            var builder = activity?.let { it1 -> AlertDialog.Builder(it1,R.style.CustomAlertDialog) }
            builder!!.setView(dialogview)
            var alertDialog = builder.show()

            dialogview.ivclose.setOnClickListener {
                alertDialog.dismiss()
            }
        }

        hygiene_fluoride_goal.setOnClickListener {

            var dialogview = LayoutInflater.from(activity).inflate(R.layout.fragment_hygiene__fluoride__goal,null)
            var builder = activity?.let { it1 -> AlertDialog.Builder(it1,R.style.CustomAlertDialog) }
            builder!!.setView(dialogview)
            var alertDialog = builder.show()

            dialogview.ivclose.setOnClickListener {
                alertDialog.dismiss()
            }
        }

        hygiene_fluoride_comparison.setOnClickListener {

            var dialogview = LayoutInflater.from(activity).inflate(R.layout.fragment_hygiene_fluoride__comparison,null)
            var builder = activity?.let { it1 -> AlertDialog.Builder(it1,R.style.CustomAlertDialog) }
            builder!!.setView(dialogview)
            var alertDialog = builder.show()

            dialogview.ivclose.setOnClickListener {
                alertDialog.dismiss()
            }
        }
        return view
    }
}