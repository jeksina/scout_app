package com.example.scout

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.activity.OnBackPressedCallback
import androidx.navigation.fragment.findNavController

class Change_PassWorldFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view :View = inflater.inflate(R.layout.fragment_change__pass_world, container, false)

        var Back_Arrow = view.findViewById<ImageView>(R.id.ivChangePass_backarrow)

        Back_Arrow.setOnClickListener {

            var action = Change_PassWorldFragmentDirections.actionChangePassWorldFragmentToSettingFragment()
            findNavController().navigate(action)
        }

        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                var action = Change_PassWorldFragmentDirections.actionChangePassWorldFragmentToSettingFragment()
                findNavController().navigate(action)
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner,callback)


        return view
    }
}