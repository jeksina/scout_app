package com.example.scout

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.FragmentTransaction
import androidx.navigation.fragment.findNavController

class MyProfielFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view :View = inflater.inflate(R.layout.fragment_my_profiel, container, false)

        var My_Practice = view.findViewById<LinearLayout>(R.id.mypractice)
        var Back_Arrow = view.findViewById<ImageView>(R.id.ivProfile_backarrow)

        My_Practice.setOnClickListener {

            var action = MyProfielFragmentDirections.actionMyProfielFragmentToMyPracticeFragment()
            findNavController().navigate(action)
        }

        Back_Arrow.setOnClickListener {

                    var action = MyProfielFragmentDirections.actionMyProfielFragmentToMenuFragment3()
                    findNavController().navigate(action)
                }

        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                var action = MyProfielFragmentDirections.actionMyProfielFragmentToMenuFragment3()
                findNavController().navigate(action)
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner,callback)


        return view
    }

}