package com.example.scout

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.scout.Data.UserDao
import com.example.scout.Data.UserDatabase
import com.example.scout.Data.UserEntity
import kotlinx.android.synthetic.main.fragment_log_in.*


class LogInFragment : Fragment() {

    private lateinit var userDatabse : UserDatabase

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view :View = inflater.inflate(R.layout.fragment_log_in, container, false)

        var SignUp = view.findViewById<TextView>(R.id.L_SignUp)
        var Next = view.findViewById<TextView>(R.id.btnLogInNext)
        var Forgot_Password = view.findViewById<TextView>(R.id.tvForgot_Password)

        Next.setOnClickListener {


            insertData()
        }

        SignUp.setOnClickListener {

            var action = LogInFragmentDirections.actionLogInFragmentToSignUpFragment()
            findNavController().navigate(action)
        }

        Forgot_Password.setOnClickListener {

            var action = LogInFragmentDirections.actionLogInFragmentToForgotPasswordFragment()
            findNavController().navigate(action)
        }

        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                val a = Intent(Intent.ACTION_MAIN)
                a.addCategory(Intent.CATEGORY_HOME)
                a.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(a)
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner,callback)

        return view
    }

    private fun insertData() {
        var emailAddress = etLogIn_email.text.toString()
        var password = etLogIn_Password.text.toString()

        if (emailAddress.isEmpty() || password.isEmpty()){
            Toast.makeText(activity,"Fill all th Fields!!",Toast.LENGTH_SHORT).show()
        }else{
            userDatabse = activity?.let { UserDatabase.getDatabase(it) }!!
            var userDao = userDatabse.userDao()

            Thread(Runnable {
                var userEntity = userDao.login(emailAddress,password)
                if (userEntity == null){
                    requireActivity().runOnUiThread(kotlinx.coroutines.Runnable {

                        Toast.makeText(activity,"InValid Email Address Or Password",Toast.LENGTH_SHORT).show()

                    })
                }else{
                    var action = LogInFragmentDirections.actionLogInFragmentToMenuFragment()
                    findNavController().navigate(action)

                }
            }).start()
        }

    }

}