package com.example.scout

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.FragmentTransaction
import androidx.navigation.fragment.findNavController

class MyPracticeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view :View = inflater.inflate(R.layout.fragment_my_practice, container, false)

        var Back_Arrow = view.findViewById<ImageView>(R.id.ivMyPractice_backarrow)

        Back_Arrow.setOnClickListener {

            var action = MyPracticeFragmentDirections.actionMyPracticeFragmentToMyProfielFragment2()
            findNavController().navigate(action)
        }

        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                var action = MyPracticeFragmentDirections.actionMyPracticeFragmentToMyProfielFragment2()
                findNavController().navigate(action)
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner,callback)
        return view
    }
}