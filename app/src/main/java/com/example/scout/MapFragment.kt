package com.example.scout

import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationListener
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.SearchView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.fragment.app.FragmentActivity
import androidx.navigation.fragment.findNavController
import com.github.florent37.runtimepermission.kotlin.askPermission
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.fragment_map.*
import java.io.IOException
import java.util.*

class MapFragment : Fragment(), OnMapReadyCallback{

    private var map : GoogleMap? = null
    lateinit var mapView: MapView
    private var MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var view :View = inflater.inflate(R.layout.fragment_map, container, false)

        var Next = view.findViewById<TextView>(R.id.btnMapNext)
        var Address = view.findViewById<SearchView>(R.id.etAddrress)


        Next.setOnClickListener {

            var action = MapFragmentDirections.actionMapFragmentToMenuFragment()
            findNavController().navigate(action)
        }

        var mapViewBundle: Bundle?= null
        if (savedInstanceState != null){
            mapViewBundle = savedInstanceState.getBundle(MAP_VIEW_BUNDLE_KEY)
        }

        mapView = view.findViewById<MapView>(R.id.mapview)
        mapView.onCreate(mapViewBundle)
        mapView.getMapAsync(this)

        Address.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(p0: String?): Boolean {

                var location = Address.query.toString()
                var addresslist : List<Address>? = null

                if (location != null) {

                    var gecoder: Geocoder = Geocoder(activity)

                    try {
                        addresslist = gecoder.getFromLocationName(location, 1)

                        map!!.clear()
                        var address: Address = addresslist!!.get(0)
                        var latLng: LatLng = LatLng(address.latitude, address.longitude)
                        map!!.addMarker(MarkerOptions().position(latLng).title(location))
                        map!!.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10.0F))

                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }

                return false
            }

            override fun onQueryTextChange(p0: String?): Boolean {
                return false
            }

        })

        return view
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        mapView.onResume()
        map = googleMap

        if (activity?.let {
                ActivityCompat.checkSelfPermission(
                    it,
                    android.Manifest.permission.ACCESS_FINE_LOCATION
                )
            } != PackageManager.PERMISSION_GRANTED && activity?.let {
                ActivityCompat.checkSelfPermission(
                    it,
                    android.Manifest.permission.ACCESS_COARSE_LOCATION
                )
            } != PackageManager.PERMISSION_GRANTED
        ){
            return
        }
        map!!.isMyLocationEnabled = true
    }

    public override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        askRunTimePermisstionLocation()

        var mapViewBundle = outState.getBundle(MAP_VIEW_BUNDLE_KEY)
        if (mapViewBundle == null){
            mapViewBundle = Bundle()
            outState.putBundle(MAP_VIEW_BUNDLE_KEY,mapViewBundle)
        }

        mapView.onSaveInstanceState(mapViewBundle)
    }


    private fun askRunTimePermisstionLocation(){
        askPermission(
            android.Manifest.permission.ACCESS_FINE_LOCATION,
            android.Manifest.permission.ACCESS_COARSE_LOCATION
        ){

        }.onDeclined { e ->
            if (e.hasDenied()){
                e.denied.forEach {

                }

                activity?.let {
                    AlertDialog.Builder(it)
                        .setMessage("Please access our permission..otherwise you will not be able to use some of our Impotant Feature")
                        .setPositiveButton("Yes"){_,_ ->
                            e.askAgain()
                        }
                        .setNegativeButton("No"){dialog,_ ->
                            dialog.dismiss()
                        }
                        .show()
                }
            }

            if (e.hasForeverDenied()){
                e.foreverDenied.forEach {

                }
                e.goToSettings()
            }
        }
    }

}

