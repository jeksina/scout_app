package com.example.scout.CollectionFragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.navigation.fragment.findNavController
import androidx.viewpager.widget.ViewPager
import com.example.scout.R
import com.google.android.material.tabs.TabLayout
import com.jaredrummler.materialspinner.MaterialSpinner

class CollectionFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view :View = inflater.inflate(R.layout.fragment_collection, container, false)

        var Collection_calender = view.findViewById<LinearLayout>(R.id.tvCollection_Calender)
        var Collectionimage_calender = view.findViewById<ImageView>(R.id.ivCollection_Calender)
        var Collection_spinner = view.findViewById<MaterialSpinner>(R.id.spCollection_Calender)

        var View_Pager = view.findViewById<ViewPager>(R.id.Collection_viewPager)
        var Tab = view.findViewById<TabLayout>(R.id.Patient_tabs)

        var Collection_mon = view.findViewById<LinearLayout>(R.id.Collection_mon)
        var Collection_tue = view.findViewById<LinearLayout>(R.id.Collection_tue)
        var Collection_wed = view.findViewById<LinearLayout>(R.id.Collection_wed)
        var Collection_thurs = view.findViewById<LinearLayout>(R.id.Collection_thurs)
        var Collection_fri = view.findViewById<LinearLayout>(R.id.Collection_fri)
        var Collection_sat = view.findViewById<LinearLayout>(R.id.Collection_sat)
        var Collection_sun = view.findViewById<LinearLayout>(R.id.Collection_sun)

        var Back = view.findViewById<ImageView>(R.id.Collection_Back)

        Back.setOnClickListener {

            var action =
                CollectionFragmentDirections.actionCollectionFragmentToMenuFragment()
            findNavController().navigate(action)
        }


        Collection_calender.visibility = View.GONE

        Collectionimage_calender.setOnClickListener{
            Collectionimage_calender.setColorFilter(resources.getColor(R.color.green))
            Collection_calender.visibility = View.VISIBLE

            val SPINNER_DATA = arrayOf("Month", "Month to date", "Last Month", "Year to date" , "Custom")

            val adapter: ArrayAdapter<String>? = this!!.activity?.let { it1 ->
                ArrayAdapter<String>(
                    it1,
                    android.R.layout.simple_dropdown_item_1line,
                    SPINNER_DATA
                )
            }

            if (adapter != null) {
                Collection_spinner.setAdapter(adapter)
            }
        }

        val adapter =
            MyViewPagerAdapter(
                childFragmentManager
            )
        adapter.addFragment(CollectionDataFragment(), " All Provider ")
        adapter.addFragment(CollectionDataFragment(), " Dr.John ")
        adapter.addFragment(CollectionDataFragment(), " Dr.Smith ")
        adapter.addFragment(CollectionDataFragment(), "Dr.Williab ")
        View_Pager.adapter = adapter
        Tab.setupWithViewPager(View_Pager)

        Collection_mon.setOnClickListener {
            Collection_mon.setBackgroundResource(R.drawable.round_border);
            Collection_tue.setBackgroundResource(R.color.calender_back);
            Collection_wed.setBackgroundResource(R.color.calender_back);
            Collection_thurs.setBackgroundResource(R.color.calender_back);
            Collection_fri.setBackgroundResource(R.color.calender_back);
            Collection_sat.setBackgroundResource(R.color.calender_back);
            Collection_sun.setBackgroundResource(R.color.calender_back);
        }

        Collection_tue.setOnClickListener {
            Collection_mon.setBackgroundResource(R.color.calender_back);
            Collection_tue.setBackgroundResource(R.drawable.round_border);
            Collection_wed.setBackgroundResource(R.color.calender_back);
            Collection_thurs.setBackgroundResource(R.color.calender_back);
            Collection_fri.setBackgroundResource(R.color.calender_back);
            Collection_sat.setBackgroundResource(R.color.calender_back);
            Collection_sun.setBackgroundResource(R.color.calender_back);
        }

        Collection_wed.setOnClickListener {
            Collection_mon.setBackgroundResource(R.color.calender_back);
            Collection_tue.setBackgroundResource(R.color.calender_back);
            Collection_wed.setBackgroundResource(R.drawable.round_border);
            Collection_thurs.setBackgroundResource(R.color.calender_back);
            Collection_fri.setBackgroundResource(R.color.calender_back);
            Collection_sat.setBackgroundResource(R.color.calender_back);
            Collection_sun.setBackgroundResource(R.color.calender_back);
        }

        Collection_thurs.setOnClickListener {
            Collection_mon.setBackgroundResource(R.color.calender_back);
            Collection_tue.setBackgroundResource(R.color.calender_back);
            Collection_wed.setBackgroundResource(R.color.calender_back);
            Collection_thurs.setBackgroundResource(R.drawable.round_border);
            Collection_fri.setBackgroundResource(R.color.calender_back);
            Collection_sat.setBackgroundResource(R.color.calender_back);
            Collection_sun.setBackgroundResource(R.color.calender_back);
        }

        Collection_fri.setOnClickListener {
            Collection_mon.setBackgroundResource(R.color.calender_back);
            Collection_tue.setBackgroundResource(R.color.calender_back);
            Collection_wed.setBackgroundResource(R.color.calender_back);
            Collection_thurs.setBackgroundResource(R.color.calender_back);
            Collection_fri.setBackgroundResource(R.drawable.round_border);
            Collection_sat.setBackgroundResource(R.color.calender_back);
            Collection_sun.setBackgroundResource(R.color.calender_back);
        }

        Collection_sat.setOnClickListener {
            Collection_mon.setBackgroundResource(R.color.calender_back);
            Collection_tue.setBackgroundResource(R.color.calender_back);
            Collection_wed.setBackgroundResource(R.color.calender_back);
            Collection_thurs.setBackgroundResource(R.color.calender_back);
            Collection_fri.setBackgroundResource(R.color.calender_back);
            Collection_sat.setBackgroundResource(R.drawable.round_border);
            Collection_sun.setBackgroundResource(R.color.calender_back);
        }

        Collection_sun.setOnClickListener {
            Collection_mon.setBackgroundResource(R.color.calender_back);
            Collection_tue.setBackgroundResource(R.color.calender_back);
            Collection_wed.setBackgroundResource(R.color.calender_back);
            Collection_thurs.setBackgroundResource(R.color.calender_back);
            Collection_fri.setBackgroundResource(R.color.calender_back);
            Collection_sat.setBackgroundResource(R.color.calender_back);
            Collection_sun.setBackgroundResource(R.drawable.round_border);
        }


        return view
    }

    class MyViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {

        private val fragmentList: MutableList<Fragment> = ArrayList()
        private val titleList: MutableList<String> = ArrayList()

        override fun getItem(position: Int): Fragment {
            return fragmentList[position]
        }

        override fun getCount(): Int {
            return fragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String) {
            fragmentList.add(fragment)
            titleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return titleList[position]
        }
    }

}