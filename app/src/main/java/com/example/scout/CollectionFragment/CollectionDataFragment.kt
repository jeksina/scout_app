package com.example.scout.CollectionFragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.appcompat.app.AlertDialog
import com.example.scout.R
import kotlinx.android.synthetic.main.fragment_collection__comparision.view.*
import kotlinx.android.synthetic.main.fragment_collection__goal.view.*
import kotlinx.android.synthetic.main.fragment_collection__per__comparison.view.*
import kotlinx.android.synthetic.main.fragment_collection__per__goal.view.*
import kotlinx.android.synthetic.main.fragment_hygiene__goal.view.*

class CollectionDataFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view :View = inflater.inflate(R.layout.fragment_collection_data, container, false)

        var collection_goal = view.findViewById<ProgressBar>(R.id.collection_goal)
        var collection_comparison = view.findViewById<ProgressBar>(R.id.collection_Comparison)
        var collection_per_goal = view.findViewById<ProgressBar>(R.id.collection_per_goal)
        var collection_per_comparison = view.findViewById<ProgressBar>(R.id.collection_per_comparison)
        var collection_insurance_goal = view.findViewById<ProgressBar>(R.id.collection_insurance_goal)
        var collection_insurance_comparison = view.findViewById<ProgressBar>(R.id.collection_insurance_comparison)
        var collection_patient_goal = view.findViewById<ProgressBar>(R.id.collection_patient_goal)
        var collection_patient_comparison = view.findViewById<ProgressBar>(R.id.collection_patient_comparison)

        collection_goal.setOnClickListener {

            var dialogview = LayoutInflater.from(activity).inflate(R.layout.fragment_collection__goal,null)
            var builder = activity?.let { it1 -> AlertDialog.Builder(it1,
                R.style.CustomAlertDialog
            ) }
            builder!!.setView(dialogview)
            var alertDialog = builder.show()

            dialogview.collection__goal_close.setOnClickListener {
                alertDialog.dismiss()
            }
        }

        collection_comparison.setOnClickListener {

            var dialogview = LayoutInflater.from(activity).inflate(R.layout.fragment_collection__comparision,null)
            var builder = activity?.let { it1 -> AlertDialog.Builder(it1,
                R.style.CustomAlertDialog
            ) }
            builder!!.setView(dialogview)
            var alertDialog = builder.show()

            dialogview.collection__comparision_close.setOnClickListener {
                alertDialog.dismiss()
            }
        }

        collection_per_goal.setOnClickListener {

            var dialogview = LayoutInflater.from(activity).inflate(R.layout.fragment_collection__per__goal,null)
            var builder = activity?.let { it1 -> AlertDialog.Builder(it1,
                R.style.CustomAlertDialog
            ) }
            builder!!.setView(dialogview)
            var alertDialog = builder.show()

            dialogview.collection__per__goal_close.setOnClickListener {
                alertDialog.dismiss()
            }
        }

        collection_per_comparison.setOnClickListener {

            var dialogview = LayoutInflater.from(activity).inflate(R.layout.fragment_collection__per__comparison,null)
            var builder = activity?.let { it1 -> AlertDialog.Builder(it1,
                R.style.CustomAlertDialog
            ) }
            builder!!.setView(dialogview)
            var alertDialog = builder.show()

            dialogview.collection_per__comparison_close.setOnClickListener {
                alertDialog.dismiss()
            }
        }

        collection_patient_goal.setOnClickListener {

            var dialogview = LayoutInflater.from(activity).inflate(R.layout.fragment_collection__patient__goal,null)
            var builder = activity?.let { it1 -> AlertDialog.Builder(it1,
                R.style.CustomAlertDialog
            ) }
            builder!!.setView(dialogview)
            var alertDialog = builder.show()

            dialogview.ivclose.setOnClickListener {
                alertDialog.dismiss()
            }
        }

        collection_patient_comparison.setOnClickListener {

            var dialogview = LayoutInflater.from(activity).inflate(R.layout.fragment_collection__patient__comparison,null)
            var builder = activity?.let { it1 -> AlertDialog.Builder(it1,
                R.style.CustomAlertDialog
            ) }
            builder!!.setView(dialogview)
            var alertDialog = builder.show()

            dialogview.ivclose.setOnClickListener {
                alertDialog.dismiss()
            }
        }

        collection_insurance_goal.setOnClickListener {

            var dialogview = LayoutInflater.from(activity).inflate(R.layout.fragment_collection__insurance__goal,null)
            var builder = activity?.let { it1 -> AlertDialog.Builder(it1,
                R.style.CustomAlertDialog
            ) }
            builder!!.setView(dialogview)
            var alertDialog = builder.show()

            dialogview.ivclose.setOnClickListener {
                alertDialog.dismiss()
            }
        }

        collection_insurance_comparison.setOnClickListener {

            var dialogview = LayoutInflater.from(activity).inflate(R.layout.fragment_collection__insurance__comparison,null)
            var builder = activity?.let { it1 -> AlertDialog.Builder(it1,
                R.style.CustomAlertDialog
            ) }
            builder!!.setView(dialogview)
            var alertDialog = builder.show()

            dialogview.ivclose.setOnClickListener {
                alertDialog.dismiss()
            }
        }
        return view
    }
}