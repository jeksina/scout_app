package com.example.scout

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.navigation.fragment.findNavController
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.jaredrummler.materialspinner.MaterialSpinner

class NewPatientFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view :View = inflater.inflate(R.layout.fragment_new_patient, container, false)

        var Patient_calender = view.findViewById<LinearLayout>(R.id.tvPatient_Calender)
        var Patientimage_calender = view.findViewById<ImageView>(R.id.ivPatient_Calender)
        var PPatient_spinner = view.findViewById<MaterialSpinner>(R.id.spPatient_Calender)

        var View_Pager = view.findViewById<ViewPager>(R.id.Patient_viewPager)
        var Tab = view.findViewById<TabLayout>(R.id.Patient_tabs)

        var Patient_mon = view.findViewById<LinearLayout>(R.id.Patient_mon)
        var Patient_tue = view.findViewById<LinearLayout>(R.id.Patient_tue)
        var Patient_wed = view.findViewById<LinearLayout>(R.id.Patient_wed)
        var Patient_thurs = view.findViewById<LinearLayout>(R.id.Patient_thurs)
        var Patient_fri = view.findViewById<LinearLayout>(R.id.Patient_fri)
        var Patient_sat = view.findViewById<LinearLayout>(R.id.Patient_sat)
        var Patient_sun = view.findViewById<LinearLayout>(R.id.Patient_sun)

        var Back = view.findViewById<ImageView>(R.id.Patient_Back)

        Back.setOnClickListener {

            var action = NewPatientFragmentDirections.actionNewPatientFragmentToMenuFragment()
            findNavController().navigate(action)
        }


        Patient_calender.visibility = View.GONE

        Patientimage_calender.setOnClickListener{
            Patientimage_calender.setColorFilter(resources.getColor(com.example.scout.R.color.green))
            Patient_calender.visibility = View.VISIBLE

            val SPINNER_DATA = arrayOf("Month", "Month to date", "Last Month", "Year to date" , "Custom")

            val adapter: ArrayAdapter<String>? = this!!.activity?.let { it1 ->
                ArrayAdapter<String>(
                    it1,
                    android.R.layout.simple_dropdown_item_1line,
                    SPINNER_DATA
                )
            }

            if (adapter != null) {
                PPatient_spinner.setAdapter(adapter)
            }
        }

        val adapter = MyViewPagerAdapter(childFragmentManager)
        adapter.addFragment(NewPatientDataFragment() , " All Provider ")
        adapter.addFragment(NewPatientDataFragment() , " Dr.John ")
        adapter.addFragment(NewPatientDataFragment() , " Dr.Smith ")
        adapter.addFragment(NewPatientDataFragment() , "Dr.Williab ")
        View_Pager.adapter = adapter
        Tab.setupWithViewPager(View_Pager)

        Patient_mon.setOnClickListener {
            Patient_mon.setBackgroundResource(com.example.scout.R.drawable.round_border);
            Patient_tue.setBackgroundResource(com.example.scout.R.color.calender_back);
            Patient_wed.setBackgroundResource(com.example.scout.R.color.calender_back);
            Patient_thurs.setBackgroundResource(com.example.scout.R.color.calender_back);
            Patient_fri.setBackgroundResource(com.example.scout.R.color.calender_back);
            Patient_sat.setBackgroundResource(com.example.scout.R.color.calender_back);
            Patient_sun.setBackgroundResource(com.example.scout.R.color.calender_back);
        }

        Patient_tue.setOnClickListener {
            Patient_mon.setBackgroundResource(com.example.scout.R.color.calender_back);
            Patient_tue.setBackgroundResource(com.example.scout.R.drawable.round_border);
            Patient_wed.setBackgroundResource(com.example.scout.R.color.calender_back);
            Patient_thurs.setBackgroundResource(com.example.scout.R.color.calender_back);
            Patient_fri.setBackgroundResource(com.example.scout.R.color.calender_back);
            Patient_sat.setBackgroundResource(com.example.scout.R.color.calender_back);
            Patient_sun.setBackgroundResource(com.example.scout.R.color.calender_back);
        }

        Patient_wed.setOnClickListener {
            Patient_mon.setBackgroundResource(com.example.scout.R.color.calender_back);
            Patient_tue.setBackgroundResource(com.example.scout.R.color.calender_back);
            Patient_wed.setBackgroundResource(com.example.scout.R.drawable.round_border);
            Patient_thurs.setBackgroundResource(com.example.scout.R.color.calender_back);
            Patient_fri.setBackgroundResource(com.example.scout.R.color.calender_back);
            Patient_sat.setBackgroundResource(com.example.scout.R.color.calender_back);
            Patient_sun.setBackgroundResource(com.example.scout.R.color.calender_back);
        }

        Patient_thurs.setOnClickListener {
            Patient_mon.setBackgroundResource(com.example.scout.R.color.calender_back);
            Patient_tue.setBackgroundResource(com.example.scout.R.color.calender_back);
            Patient_wed.setBackgroundResource(com.example.scout.R.color.calender_back);
            Patient_thurs.setBackgroundResource(com.example.scout.R.drawable.round_border);
            Patient_fri.setBackgroundResource(com.example.scout.R.color.calender_back);
            Patient_sat.setBackgroundResource(com.example.scout.R.color.calender_back);
            Patient_sun.setBackgroundResource(com.example.scout.R.color.calender_back);
        }

        Patient_fri.setOnClickListener {
            Patient_mon.setBackgroundResource(com.example.scout.R.color.calender_back);
            Patient_tue.setBackgroundResource(com.example.scout.R.color.calender_back);
            Patient_wed.setBackgroundResource(com.example.scout.R.color.calender_back);
            Patient_thurs.setBackgroundResource(com.example.scout.R.color.calender_back);
            Patient_fri.setBackgroundResource(com.example.scout.R.drawable.round_border);
            Patient_sat.setBackgroundResource(com.example.scout.R.color.calender_back);
            Patient_sun.setBackgroundResource(com.example.scout.R.color.calender_back);
        }

        Patient_sat.setOnClickListener {
            Patient_mon.setBackgroundResource(com.example.scout.R.color.calender_back);
            Patient_tue.setBackgroundResource(com.example.scout.R.color.calender_back);
            Patient_wed.setBackgroundResource(com.example.scout.R.color.calender_back);
            Patient_thurs.setBackgroundResource(com.example.scout.R.color.calender_back);
            Patient_fri.setBackgroundResource(com.example.scout.R.color.calender_back);
            Patient_sat.setBackgroundResource(com.example.scout.R.drawable.round_border);
            Patient_sun.setBackgroundResource(com.example.scout.R.color.calender_back);
        }

        Patient_sun.setOnClickListener {
            Patient_mon.setBackgroundResource(com.example.scout.R.color.calender_back);
            Patient_tue.setBackgroundResource(com.example.scout.R.color.calender_back);
            Patient_wed.setBackgroundResource(com.example.scout.R.color.calender_back);
            Patient_thurs.setBackgroundResource(com.example.scout.R.color.calender_back);
            Patient_fri.setBackgroundResource(com.example.scout.R.color.calender_back);
            Patient_sat.setBackgroundResource(com.example.scout.R.color.calender_back);
            Patient_sun.setBackgroundResource(com.example.scout.R.drawable.round_border);
        }

        return view
    }

    class MyViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {

        private val fragmentList: MutableList<Fragment> = ArrayList()
        private val titleList: MutableList<String> = ArrayList()

        override fun getItem(position: Int): Fragment {
            return fragmentList[position]
        }

        override fun getCount(): Int {
            return fragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String) {
            fragmentList.add(fragment)
            titleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return titleList[position]
        }
    }
}