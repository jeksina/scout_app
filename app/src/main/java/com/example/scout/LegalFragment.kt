package com.example.scout

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.activity.OnBackPressedCallback
import androidx.navigation.fragment.findNavController

class LegalFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view :View = inflater.inflate(R.layout.fragment_legal, container, false)

        var Term_Condition = view.findViewById<LinearLayout>(R.id.term_condition)
        var Privacy = view.findViewById<LinearLayout>(R.id.privacy_policy)
        var Back_Arrow = view.findViewById<ImageView>(R.id.ivLegal_backarrow)

        Term_Condition.setOnClickListener {

            var action = LegalFragmentDirections.actionLegalFragmentToTermConditionFragment()
            findNavController().navigate(action)
        }

        Privacy.setOnClickListener {

            var action = LegalFragmentDirections.actionLegalFragmentToPrivacyFragment()
            findNavController().navigate(action)
        }

        Back_Arrow.setOnClickListener {

            var action = LegalFragmentDirections.actionLegalFragmentToMenuFragment()
            findNavController().navigate(action)
        }

        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                var action = LegalFragmentDirections.actionLegalFragmentToMenuFragment()
                findNavController().navigate(action)
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner,callback)

        return view
    }
}